# Information
`superCereal` is a package to produce graph kernels for the Cluster Editing problem.

This package contains:

* A dynamic graph data structure based on adjacency lists.
* Several engineered kernelization algorithms for the Cluster Editing problem, including:
	* [Edge cuts](https://link.springer.com/chapter/10.1007/978-3-642-17493-3_8)
	* [The k+1 rule](https://link.springer.com/chapter/10.1007/978-3-319-19084-6_5)
	* [P3 free editing](https://link.springer.com/chapter/10.1007/978-3-319-34171-2_5)
* A solution lifting algorithm that uses a solution of a kernel to build a solution for the original graph. The solution for the original graph is optimal, iff the solution for the kernel was optimal.

# Installation
The program is written in the [Rust programming language](https://www.rust-lang.org/).

Install instructions can be found here: <https://www.rust-lang.org/tools/install>.
Following these instructions will install the Rust compiler (`rustc`) and Rust's package manager `cargo`.


# Dependencies
The only (direct) dependency is a package ("crate") called `rand` ([Description](https://github.com/rust-random/rand), [Code](https://github.com/rust-random/rand)), used for pseudorandom number generation.

Dependencies will be automatically downloaded and compiled, when you use `cargo` to compile the program.

# Compiling
Running `cargo build --release` will build the library, along with the binaries.
This automatically does the appropriate `rustc` calls.
We compiled using rustc version 1.49.0.

# Running
Run `cargo run --release --bin kernelize_ec` to execute the binary that produces a kernel.
Run `cargo run --release --bin lift` to execute the binary that provides the solution lifting.

Alternatively, the compiled binaries, found in `target/release/`, can be called directly.

# Algorithm description
A description of our method is provided in this repository in `description.pdf`.

