//! Compute an upper bound for an Cluster Editing Instance.
//!
//! The best way is to construct an [`UpperBound`] instance and
//! use its methods to determine an upper bound.
//!
//! # Example
//!
//! ```
//! use std::io::Cursor;
//! use kernel_impl::alt_graph::UGraph;
//! use kernel_impl::bounds::UpperBound;
//!
//! // C_4: Cycle graph with four nodes.
//! let gr = Cursor::new("p cep 4 4\n1 2\n2 3\n3 4\n4 1\n");
//! let graph = UGraph::read_gr(gr).unwrap();
//! let mut b = UpperBound::new(&graph);
//! b.compute_default_bounds();
//!
//! // Now inspect what we've got.
//! let upper_bound = b.upper_bound();
//! assert_eq!(upper_bound, 2);
//! let cluster_graph = b.cluster_graph();
//! assert_eq!(cluster_graph.num_edges(), 6);
//! ```
use crate::alt_graph::UGraph;
use crate::edge_cuts::CriticalCliques;

use std::collections::{HashMap, HashSet};

use rand::{RngCore, seq::{IteratorRandom, SliceRandom}};

/// Used to specify the order the vertices should be considered
/// when calling [`UpperBound::greedy()`].
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum VertexOrder {
	/// Vertex identifier in ascending order
	Id,
	/// Vertex identifier in descending order
	IdRev,
	/// Vertex degree in ascending order
	Degree,
	/// Vertex degree in descending order
	DegreeRev,
	/// Vertex degree (updated during the run) in ascending order
	DegreeDyn,
	/// Vertex degree (updated during the run) in descending order
	DegreeDynRev,
	/// Random order
	Random,
}
#[derive(Clone, Debug)]
enum ClusterGraphRepr {
	Complete,
	Empty,
	TwoClusters(usize),
	Clusters(Vec<HashSet<usize>>),
	//Graph(UGraph),
}
impl Default for ClusterGraphRepr {
	fn default() -> Self {
		Self::Empty
	}
}
#[derive(Clone, Debug)]
struct ClusterGraph {
	cluster_indices: Vec<usize>,
	clusters: Vec<HashSet<usize>>,
	size: usize,
}
impl ClusterGraph {
	pub fn from_repr(repr: &ClusterGraphRepr, graph: &UGraph) -> Self {
		let size = graph.adj_list_len();
		let clusters = match repr {
			ClusterGraphRepr::Clusters(c) => c.clone(),
			ClusterGraphRepr::TwoClusters(i) => Self::two_clusters(*i, graph),
			ClusterGraphRepr::Complete => vec![{
				let mut c = HashSet::new();
				c.extend(graph.nodes());
				c
			}],
			ClusterGraphRepr::Empty => graph.nodes().map(|i| {
				let mut c = HashSet::with_capacity(1);
				c.insert(i);
				c
			}).collect(),
		};
		Self::from_clusters(clusters, size)
	}
	// Nodes not in the union of the HashSets are assumed to be deleted.
	fn from_clusters(clusters: Vec<HashSet<usize>>, size: usize) -> Self {
		let mut cluster_indices = vec![999777999; size];
		for (i, c) in clusters.iter().enumerate() {
			for v in c {
				cluster_indices[*v] = i;
			}
		}
		Self {
			cluster_indices,
			clusters,
			size,
		}
	}

	fn two_clusters(i: usize, graph: &UGraph) -> Vec<HashSet<usize>> {
		let c1 = {
			let mut n = graph.open_neighborhood(i).clone().expect("node i exists");
			n.insert(i);
			n
		};
		let c2 = graph.nodes().filter(|v| !c1.contains(v)).collect();

		vec![c1, c2]
	}

	pub fn len(&self) -> usize {
		self.clusters.len()
	}

	pub fn to_graph(&self) -> UGraph {
		UGraph::with_clusters(self.size, &self.clusters)
	}

	pub fn cluster_id(&self, node: usize) -> usize {
		self.cluster_indices[node]
	}
	// Not used at the moment
	//pub fn same_cluster(&self, node1: usize, node2: usize) -> bool {
	//	self.cluster_indices[node1] == self.cluster_indices[node2]
	//}
	//pub fn cluster_nodes(&self, cluster_id: usize) -> &HashSet<usize> {
	//	&self.clusters[cluster_id]
	//}

	/// Puts `node` into the cluster (might be a new singleton) that improves
	/// the number of edits the most.
	fn reassign_cluster(&mut self, node: usize, graph: &UGraph) -> usize {
		let old_cluster = self.cluster_id(node);
		// The number of neighbors `node` has in each of the clusters
		let mut cluster_neighbors = vec![0; self.clusters.len()];

		for v in graph.open_neighborhood(node).as_ref().expect("node exists") {
			cluster_neighbors[self.cluster_id(*v)] += 1;
		}

		// The number of non-neighbors `node` has in each of the clusters.
		let cluster_nonneighbors: Vec<_> = cluster_neighbors.iter()
			.zip(self.clusters.iter())
			.enumerate()
			.map(|(i, (count, cluster))| {
				cluster.len() - count - if i == old_cluster { 1 } else { 0 }
			})
			.collect();

		let own_cluster_cost = cluster_nonneighbors[old_cluster] as isize - cluster_neighbors[old_cluster] as isize;
		let mut new_cluster = None;
		let mut found_better = own_cluster_cost > 0;
		let mut improvement = own_cluster_cost.max(0);
		for i in 0..self.clusters.len() {
			let e = cluster_neighbors[i] as isize;
			let a = cluster_nonneighbors[i] as isize;
			let imp = own_cluster_cost + e - a;

			if imp > improvement {
				found_better = true;
				new_cluster = Some(i);
				improvement = imp;
			}
		}

		if found_better {
			if let Some(c) = new_cluster {
				// Put `node` into this cluster.
				self.cluster_indices[node] = c;
				self.clusters[old_cluster].remove(&node);
				self.clusters[c].insert(node);
			} else {
				// Put `node` into its own singleton cluster i.e., isolate `node`.
				self.cluster_indices[node] = self.clusters.len();
				self.clusters[old_cluster].remove(&node);
				let mut new_cluster = HashSet::new();
				new_cluster.insert(node);
				self.clusters.push(new_cluster);
			}
		}

		improvement as usize
	}

	/*
	/// Merges two clusters if it means an improvement.
	/// Returns the achieved improvment.
	fn maybe_merge_clusters(&mut self, cluster_a: usize, cluster_b: usize, g: &UGraph) -> usize {
		let mut edges = 0;
		for &v in &self.clusters[cluster_a] {
			edges += g.open_neighborhood(v).as_ref()
				.unwrap()
				.into_iter()
				.filter(|&w| self.cluster_indices[*w] == cluster_b)
				.count();
		}
		let antiedges = self.clusters[cluster_a].len() * self.clusters[cluster_b].len() - edges;

		if edges > antiedges {
			// the merge pays off, do it!
			let mut cluster_a = cluster_a;
			let mut cluster_b = cluster_b;
			if cluster_a > cluster_b { std::mem::swap(&mut cluster_a, &mut cluster_b); }
			// we need mutable borrows at two indices of self.clusters
			let (v0, v1) = self.clusters.split_at_mut(cluster_b);
			let c = &mut v0[cluster_a];
			for v in v1[0].drain() {
				c.insert(v);
				self.cluster_indices[v] = cluster_a;
			}

			edges - antiedges
		} else {
			0
		}
	}*/
	/// Merges a cluster with another one if it means an improvement.
	/// The algorithm choosees the other cluster such that the improvement is maximized.
	/// Returns the achieved improvment.
	fn maybe_merge_cluster(&mut self, cluster_a: usize, g: &UGraph) -> usize {
		let mut edges = vec![0isize; self.clusters.len()];
		for &v in &self.clusters[cluster_a] {
			let neigh = g.open_neighborhood(v).as_ref().unwrap();
			for &w in neigh {
				let c = self.cluster_indices[w];
				if c != cluster_a {
					edges[c] += 1;
				}
			}
		}
		let mut best_idx = 0;
		let mut best_imp = 0;

		for i in 0..self.clusters.len() {
			let improvement = 2 * edges[i] - self.clusters[cluster_a].len() as isize * self.clusters[i].len() as isize;
			if improvement > best_imp {
				best_idx = i;
				best_imp = improvement;
			}
		}

		if best_imp > 0 {
			// the merge pays off, do it!
			let mut cluster_a = cluster_a;
			let mut cluster_b = best_idx;
			if cluster_a > cluster_b { std::mem::swap(&mut cluster_a, &mut cluster_b); }
			// we need mutable borrows at two indices of self.clusters
			let (v0, v1) = self.clusters.split_at_mut(cluster_b);
			let c = &mut v0[cluster_a];
			for v in v1[0].drain() {
				c.insert(v);
				self.cluster_indices[v] = cluster_a;
			}

			best_imp as usize
		} else {
			0
		}
	}
}

/// A struct for computing and storing upper bounds on the solution of a Cluster Editing instance.
#[derive(Clone, Debug)]
pub struct UpperBound<'a> {
	bound: usize,
	p_bound: usize,
	cluster_graph_r: ClusterGraphRepr,
	cluster_graph: Option<ClusterGraph>,
	graph: &'a UGraph,
}

impl<'a> UpperBound<'a> {
	/// Creates a new `UpperBound` of a graph and computes a trivial bound.
	///
	/// The trivial bound is the smaller one of the number of edges in the graph
	/// and the number of non-edges in the graph.
	/// # Example
	///
	/// ```
	/// # use std::io::Cursor;
	/// # use kernel_impl::alt_graph::UGraph;
	/// use kernel_impl::bounds::UpperBound;
	///
	/// # let gr = Cursor::new("p cep 4 4\n1 2\n2 3\n3 4\n4 1\n");
	/// # let graph = UGraph::read_gr(gr).unwrap();
	/// // let graph = ...;
	/// let mut b = UpperBound::new(&graph);
	/// b.compute_default_bounds();
	///
	/// // Now inspect what we've got.
	/// let upper_bound = b.upper_bound();
	/// let cluster_graph = b.cluster_graph();
	/// ```
	pub fn new(graph: &'a UGraph) -> Self {
		let n = graph.num_nodes();
		let m = graph.num_edges(); // HEUR1: edges
		let a = if n == 0 { 0 } else { (n-1) * n / 2 - m }; // HEUR2: anti-edges

		let (bound, cluster_graph_r) = if m <= a {
			(m, ClusterGraphRepr::Empty)
		} else {
			(a, ClusterGraphRepr::Complete)
		};

		Self {
			bound,
			p_bound: bound,
			cluster_graph_r,
			cluster_graph: None,
			graph,
		}
	}

	/// Computes some upper bounds.
	///
	/// Calls [`greedy`](UpperBound::greedy()) with various paramaters as well as
	/// [`two_clusters`](UpperBound::two_clusters()) and
	/// [`label_propagation`](UpperBound::label_propagation()).
	pub fn compute_default_bounds(&mut self) {
		use VertexOrder::*;

		self.two_clusters();
		self.label_propagation();
		for order in &[Id, IdRev, Degree, DegreeRev, Random] {
			self.greedy(*order, 0.7);
			self.greedy(*order, 0.8);
			self.greedy(*order, 0.9);
		}
	}

	/// Returns the best upper bound that has been found so far.
	pub fn upper_bound(&self) -> usize {
		self.bound
	}

	/// Returns a cluster graph that can be reached with exactly
	/// [`upper_bound`](UpperBound::upper_bound()) many edits.
	pub fn cluster_graph(&mut self) -> UGraph {
		let g = if let Some(c) = &self.cluster_graph {
			c.to_graph()
		} else {
			let c = ClusterGraph::from_repr(&self.cluster_graph_r, &self.graph);
			self.cluster_graph = Some(c);
			self.cluster_graph.as_ref().unwrap().to_graph()
		};
		g
	}

	pub fn clusters(&mut self) -> impl Iterator<Item=&HashSet<usize>> {
		if self.cluster_graph.is_none() {
			self.cluster_graph = Some(ClusterGraph::from_repr(&self.cluster_graph_r, &self.graph));
		}
		self.cluster_graph.as_ref().unwrap()
			.clusters.iter()
			.filter(|c| !c.is_empty())
	}

	pub fn local_reassignments(&mut self) -> usize {
		if self.graph.num_nodes() == 0 { return 0; }
		if self.cluster_graph.is_none() {
			self.cluster_graph = Some(ClusterGraph::from_repr(&self.cluster_graph_r, &self.graph));
		}
		let mut c = self.cluster_graph.as_mut().unwrap();

		let improvement = Self::_local_reassignments(&self.graph, &mut c);
		self.bound -= improvement;

		improvement
	}
	fn _local_reassignments(g: & UGraph, cg: &mut ClusterGraph) -> usize {
		let mut total_improvement = 0;

		let nodes: Vec<_> = g.nodes().collect();
		let mut last_improvement_at = nodes.len() - 1;
		let mut i = 0;
		while last_improvement_at != i {
			let improvement = cg.reassign_cluster(nodes[i], g);
			if improvement > 0 {
				total_improvement += improvement;
				last_improvement_at = i;
				//eprintln!("\t\tImprovement by {} at node {}", improvement, i);
			}
			i = (i + 1) % nodes.len();
		}

		total_improvement
	}
	pub fn merge_clusters(&mut self) -> usize {
		if self.graph.num_nodes() == 0 { return 0; }
		if self.cluster_graph.is_none() {
			self.cluster_graph = Some(ClusterGraph::from_repr(&self.cluster_graph_r, &self.graph));
		}
		let c = self.cluster_graph.as_mut().unwrap();

		let mut total_improvement = 0;

		for i in 0..c.len() {
			let imp = c.maybe_merge_cluster(i, &self.graph);
			total_improvement += imp;
		}
		self.bound -= total_improvement;

		total_improvement
	}

	/// Runs a greedy heuristic to obtain an upper bound.
	///
	/// The threshold should be in the intervall [0.0, 1.0].
	///
	/// This method is called by [`compute_default_bounds`](UpperBound::compute_default_bounds())
	/// with all combinations of [`Id`](VertexOrder::Id), [`IdRev`](VertexOrder::IdRev),
	/// [`Degree`](VertexOrder::Degree), [`DegreeRev`](VertexOrder::DegreeRev) and
	/// [`Random`](VertexOrder::Random) as order
	/// and 0.7, 0.8 and 0.9 as threshold.
	/// # Algorithm
	///
	/// The algorithm considers the nodes in the specified `order`.
	/// For each node `v` it does the following:
	///  * Consider the set `C` containing `v` and its neighbors.
	///  * Compute the ratio `r` of edges within `C` to the number of possible edges.
	///  * While `r < threshold`, do:
	///    - Remove the least-connected node from `C`.
	///    - Recompute `r`.
	///  * Make `C` a cluster and delete its nodes from the graph.
	pub fn greedy(&mut self, order: VertexOrder, threshold: f32) -> usize {
		assert!((0.0..=1.0).contains(&threshold));
		let mut g = self.graph.clone();

		use VertexOrder::*;
		let (b, c) = match order {
			Id => greedy_w_threshold(&mut g, threshold),
			IdRev => greedy_w_threshold_rev(&mut g, threshold),
			Degree => greedy_degree_w_threshold(&mut g, threshold),
			DegreeRev => greedy_degree_w_threshold_rev(&mut g, threshold),
			DegreeDyn => greedy_degree_dyn_w_threshold(&mut g, threshold),
			DegreeDynRev => greedy_degree_dyn_w_threshold_rev(&mut g, threshold),
			Random => greedy_random_w_threshold(&mut g, threshold),
		};

		if b < self.p_bound {
			//eprintln!("\n\tNew p_best: GREEDY {:?} t {} -> {}", order, threshold, b);
			self.p_bound = b;
			let mut cg = ClusterGraph::from_repr(&ClusterGraphRepr::Clusters(c), &self.graph);
			let improvement = Self::_local_reassignments(&self.graph, &mut cg);
			if b - improvement < self.bound {
				//eprintln!("\n\tNew best: GREEDY {:?} t {} -> {}", order, threshold, b - improvement);
				//self.cluster_graph_r = ClusterGraphRepr::Clusters(c);
				self.cluster_graph = Some(cg);
				self.bound = b - improvement;
			}
		}

		b
	}

	/// Runs an algorithm which gives a 2-approximation for a Cluster Editing problem
	/// that demands that the returned cluster graph contains exactly two clusters.
	///
	/// This method is called by [`compute_default_bounds`](UpperBound::compute_default_bounds()).
	/// # Algorithm
	///
	/// The algorithm works as follows:
	///  * Try for every node `v`:
	///    - Put `v` and its neighbors into a cluster.
	///    - Put every other node in a second cluster.
	///    - Compute the number of edits this takes.
	///  * Use the clustering with the least edits
	pub fn two_clusters(&mut self) -> usize {
		if self.graph.num_nodes() == 0 {
			return 0;
		}

		let mut best = usize::MAX;
		let mut node = 0;
		for v in self.graph.nodes() { // TODO: sample some nodes (don't test every node)
			let b = _two_clusters_helper(self.graph, v);
			if b < best {
				best = b;
				node = v;
			}
		}

		if best < self.p_bound {
			//eprintln!("\tNew p_best: 2CLUSTER -> {}", best);
			self.p_bound = best;
			let mut cg = ClusterGraph::from_repr(&ClusterGraphRepr::TwoClusters(node), &self.graph);
			let improvement = Self::_local_reassignments(&self.graph, &mut cg);
			if best - improvement < self.bound {
				//eprintln!("\tNew best: 2CLUSTER -> {}", best - improvement);
				//self.cluster_graph_r = ClusterGraphRepr::TwoClusters(node);
				self.cluster_graph = Some(cg);
				self.bound = best - improvement;
			}
		}

		best
	}

	/*
	fn two_clusters_graph(&self, v: usize) -> UGraph {
		let mut g = self.graph.clone();

		let c1 = {
			let mut n = g.open_neighborhood(v).clone().unwrap_or_else(HashSet::new);
			n.insert(v);
			n
		};
		let anti_edges = g.anti_edges_within(&c1).collect::<Vec<_>>().into_iter();
		g.insert_edges(anti_edges);

		let c2 = g.nodes().filter(|v| !c1.contains(v)).collect();
		let anti_edges = g.anti_edges_within(&c2).collect::<Vec<_>>().into_iter();
		g.insert_edges(anti_edges);

		for node in c1 {
			g.delete_edges_between(node, &c2);
		}

		g
	}
	*/

	/// Runs the heuristic "label propagation" on this graph.
	///
	/// This method is called by [`compute_default_bounds`](UpperBound::compute_default_bounds()).
	/// # Algorithm
	///
	/// This algorithm assigns a label to every node.
	/// * The initial labels are IDs for the critical clique of a node.
	/// * The labels are then iteratively changed by
	///   assigning a node the label a majority of its neighbors has.
	/// * If the labels don't change anymore the nodes are grouped into clusters
	///   according to their labels.
	pub fn label_propagation(&mut self) -> usize {
		let g = self.graph;

		// Node labels are initialized to the ID of their critical cliques
		let mut labels: Vec<usize> = {
			let cc = CriticalCliques::from_graph(g);
			(0..g.adj_list_len())
				.map(|i| { cc.clique_id(i) })
				.collect()
		};
		// update labels
		while _update_labels(g, &mut labels) {}

		// compute clusters as a Vec<HashSet<_>> from the labels
		let clusters: Vec<_> = labels.into_iter()
			.enumerate()
			.filter(|(i, _)| g.has_node(*i))
			.fold(HashMap::new(), |mut map, (i, l)| {
				// collect into `Vec`s, as `HashSet` does not impl `Hash`.
				map.entry(l).or_insert_with(Vec::new).push(i);
				map
			})
			// here we have a `HashMap<usize, Vec<usize>>`
			// let's turn it into a Vec<HashSet<usize>>`!
			.into_iter()
			.map(|(_, v)| {
				v.into_iter().collect::<HashSet<_>>()
			})
			.collect();

		let mut edits = 0;
		for c in &clusters {
			// anti-edges
			edits += g.anti_edges_within(c).count();
			// "external" edges
			for v in c {
				edits += g.open_neighborhood(*v).as_ref().unwrap()
					.iter()
					.filter(|&n| {
						v <= n && !c.contains(n)
					})
					.count();
			}
		}

		if edits < self.p_bound {
			//eprintln!("\tNew p_best: LBLPROP -> {}", edits);
			self.p_bound = edits;
			let mut cg = ClusterGraph::from_repr(&ClusterGraphRepr::Clusters(clusters), &self.graph);
			let improvement = Self::_local_reassignments(&self.graph, &mut cg);
			if edits - improvement < self.bound {
				//eprintln!("\tNew best: LBLPROP > {}", edits - improvement);
				//self.cluster_graph_r = ClusterGraphRepr::Clusters(clusters);
				self.cluster_graph = Some(cg);
				self.bound = edits - improvement;
			}
		}

		edits
	}
}

pub fn greedy(g: &mut UGraph) -> usize {
	let mut edits = 0;

	let nodes: Vec<_> = g.nodes().collect();
	for i in nodes {
		edits += _greedy_helper(g, i);
	}

	edits
}
pub fn greedy_rev(g: &mut UGraph) -> usize {
	let mut edits = 0;

	let nodes: Vec<_> = g.nodes().collect();
	for i in nodes.into_iter().rev() {
		edits += _greedy_helper(g, i);
	}

	edits
}
pub fn greedy_degree(g: &mut UGraph) -> usize {
	let mut edits = 0;
	let mut nodes: Vec<_> = g.nodes().collect();
	nodes.sort_by_key(|i| g.degree(*i));
	for i in nodes {
		edits += _greedy_helper(g, i);
	}

	edits
}
pub fn greedy_degree_rev(g: &mut UGraph) -> usize {
	let mut edits = 0;
	let mut nodes: Vec<_> = g.nodes().collect();
	nodes.sort_by_key(|i| g.degree(*i));
	for i in nodes.into_iter().rev() {
		edits += _greedy_helper(g, i);
	}

	edits
}
pub fn greedy_degree_dyn(g: &mut UGraph) -> usize {
	let mut edits = 0;

	let mut min = g.nodes()
		.filter(|i| g.degree(*i) > 0)
		.min_by_key(|i| g.degree(*i));
	while let Some(i) = min {
		edits += _greedy_helper(g, i);

		min = g.nodes()
			.filter(|i| g.degree(*i) > 0)
			.min_by_key(|i| g.degree(*i));
	}

	edits
}
pub fn greedy_degree_dyn_rev(g: &mut UGraph) -> usize {
	let mut edits = 0;
	while let Some(i) = g.nodes().max_by_key(|i| g.degree(*i)) {
		if g.degree(i) == 0 {
			break;
		}
		edits += _greedy_helper(g, i);
	}

	edits
}
fn _greedy_helper(g: &mut UGraph, i: usize) -> usize {
	let mut edits = 0;

	if let Some(mut neighbors) = g.open_neighborhood(i).clone() {
		neighbors.insert(i);
		let size = neighbors.len();
		let anti_edges = g.anti_edges_within(&neighbors).count();
		edits += anti_edges;
		for v in neighbors.iter() {
			// one edit for every adjacent node (also within neighbors)
			edits += g.degree(*v);
			g.delete_node(*v);
		}
		// subtract edges of clique (i.e. within neighbors) that were there before
		edits -= size * (size-1) / 2 - anti_edges;
	}

	edits
}

pub fn greedy_w_threshold(g: &mut UGraph, threshold: f32) -> (usize, Vec<HashSet<usize>>) {
	assert!((0.0..=1.0).contains(&threshold));
	let mut edits = 0;
	let mut clusters = Vec::new();

	let nodes: Vec<_> = g.nodes().collect();

	for i in nodes {
		if !g.has_node(i) { continue; }
		let (e, c) = _greedy_threshold_helper(g, i, threshold);
		edits += e;
		clusters.push(c);
	}

	(edits, clusters)
}
pub fn greedy_w_threshold_rev(g: &mut UGraph, threshold: f32) -> (usize, Vec<HashSet<usize>>) {
	assert!((0.0..=1.0).contains(&threshold));
	let mut edits = 0;
	let mut clusters = Vec::new();

	let nodes: Vec<_> = g.nodes().collect();

	for i in nodes.into_iter().rev() {
		if !g.has_node(i) { continue; }
		let (e, c) = _greedy_threshold_helper(g, i, threshold);
		edits += e;
		clusters.push(c);
	}

	(edits, clusters)
}
pub fn greedy_degree_w_threshold(g: &mut UGraph, threshold: f32) -> (usize, Vec<HashSet<usize>>) {
	assert!((0.0..=1.0).contains(&threshold));
	let mut edits = 0;
	let mut clusters = Vec::new();

	let mut nodes: Vec<_> = g.nodes().collect();
	nodes.sort_by_key(|i| g.degree(*i));

	for i in nodes {
		if !g.has_node(i) { continue; }
		let (e, c) = _greedy_threshold_helper(g, i, threshold);
		edits += e;
		clusters.push(c);
	}

	(edits, clusters)
}
pub fn greedy_degree_w_threshold_rev(g: &mut UGraph, threshold: f32) -> (usize, Vec<HashSet<usize>>) {
	assert!((0.0..=1.0).contains(&threshold));
	let mut edits = 0;
	let mut clusters = Vec::new();

	let mut nodes: Vec<_> = g.nodes().collect();
	nodes.sort_by_key(|i| g.degree(*i));

	for i in nodes.into_iter().rev() {
		if !g.has_node(i) { continue; }
		let (e, c) = _greedy_threshold_helper(g, i, threshold);
		edits += e;
		clusters.push(c);
	}

	(edits, clusters)
}
pub fn greedy_degree_dyn_w_threshold(g: &mut UGraph, threshold: f32) -> (usize, Vec<HashSet<usize>>) {
	assert!((0.0..=1.0).contains(&threshold));
	let mut edits = 0;
	let mut clusters = Vec::new();

	let mut min = g.nodes()
		.filter(|i| g.degree(*i) > 0)
		.min_by_key(|i| g.degree(*i));
	while let Some(i) = min {
		let (e, c) = _greedy_threshold_helper(g, i, threshold);
		edits += e;
		clusters.push(c);

		min = g.nodes()
			.filter(|i| g.degree(*i) > 0)
			.min_by_key(|i| g.degree(*i));
	}
	// add singleton clusters
	clusters.extend(g.nodes().map(|v| {
		let mut s = HashSet::new();
		s.insert(v);
		s
	}));


	(edits, clusters)
}
pub fn greedy_degree_dyn_w_threshold_rev(g: &mut UGraph, threshold: f32) -> (usize, Vec<HashSet<usize>>) {
	assert!((0.0..=1.0).contains(&threshold));
	let mut edits = 0;
	let mut clusters = Vec::new();

	while let Some(i) = g.nodes().max_by_key(|i| g.degree(*i)) {
		if g.degree(i) == 0 {
			break;
		}
		let (e, c) = _greedy_threshold_helper(g, i, threshold);
		edits += e;
		clusters.push(c);
	}
	// add singleton clusters
	clusters.extend(g.nodes().map(|v| {
		let mut s = HashSet::new();
		s.insert(v);
		s
	}));

	(edits, clusters)
}
fn greedy_random_w_threshold(g: &mut UGraph, threshold: f32) -> (usize, Vec<HashSet<usize>>) {
	assert!((0.0..=1.0).contains(&threshold));
	let mut rng = rand::thread_rng();
	let mut edits = 0;
	let mut clusters = Vec::new();

	let mut nodes: Vec<_> = g.nodes().collect();
	nodes.shuffle(&mut rng);

	for i in nodes {
		if !g.has_node(i) { continue; }
		let (e, c) = _greedy_threshold_helper(g, i, threshold);
		edits += e;
		clusters.push(c);
	}

	(edits, clusters)
}

/// Returns the number of edits and the nodes belonging to a cluster
fn _greedy_threshold_helper(g: &mut UGraph, i: usize, threshold: f32) -> (usize, HashSet<usize>) {
	assert!((0.0..=1.0).contains(&threshold));
	let mut rng = rand::thread_rng();
	let mut edits = 0;

	if let Some(mut neighbors) = g.open_neighborhood(i).clone() {
		neighbors.insert(i);

		loop {
			let size = neighbors.len();
			let pairs = size * (size-1) / 2;
			let anti_edges = g.anti_edges_within(&neighbors).count();
			let edges = pairs - anti_edges;

			if size <= 1 {
				debug_assert_eq!(size, 1);
				return (edits, neighbors);
			} else if edges as f32 / pairs as f32 >= threshold {
				// make it a clique and delete it
				edits += anti_edges;
				for v in neighbors.iter() {
					// one edit for every adjacent node (also within neighbors)
					edits += g.degree(*v);
					g.delete_node(*v);
				}
				// subtract edges of clique (i.e. within neighbors) that were there before
				edits -= size * (size-1) / 2 - anti_edges;
				return (edits, neighbors);
			} else {
				// delete node with lowest number of adjacent nodes in `neighbors`
				let x = *neighbors.iter()
					.min_by_key(|i| {
						(g.open_neighborhood(**i).as_ref().expect("node exists")
							.iter()
							.filter(|v| neighbors.contains(*v))
							.count(),
						rng.next_u32())
				}).expect("neighborhood not empty");

				edits += g.open_neighborhood(x).as_ref().expect("node exists")
					.iter()
					.filter(|v| neighbors.contains(*v))
					.count();
				g.delete_edges_between(x, &neighbors);
				neighbors.remove(&x);
			}
		}
	} else {
		(0, Default::default())
	}
}

pub fn two_clusters(g: &UGraph) -> usize {
	if g.num_nodes() == 0 {
		return 0;
	}
	let mut best = usize::MAX;
	for v in g.nodes() {
		best = best.min(_two_clusters_helper(g, v));
	}
	best
}
fn _two_clusters_helper(g: &UGraph, i: usize) -> usize {
	let mut cost = 0;
	let c1 = {
		let mut n = g.open_neighborhood(i).clone().unwrap_or(HashSet::new());
		n.insert(i);
		n
	};
	cost += g.anti_edges_within(&c1).count();

	let c2 = g.nodes().filter(|v| !c1.contains(v)).collect();
	cost += g.anti_edges_within(&c2).count();

	let inter = g.edges()
		.inspect(|(u, v)| debug_assert!((c1.contains(u) ^ c2.contains(u)) && (c1.contains(v) ^ c2.contains(v))))
		.filter(|(u, v)| c1.contains(u) ^ c1.contains(v))
		.count();
	cost += inter;

	cost
}

/// Returns if there was a label change
fn _update_labels(g: &UGraph, labels: &mut [usize]) -> bool {
	let mut rng = rand::thread_rng();
	let mut change = false;

	// TODO: random order
	for v in g.nodes() {
		let l: Vec<_> = g.open_neighborhood(v).as_ref().unwrap()
			.into_iter()
			.map(|n| labels[*n])
			.collect();
		if l.is_empty() {
			continue;
		}
		let freq = l.into_iter()
			.fold(HashMap::new(), |mut f, l| {
				*f.entry(l).or_insert(0) += 1;
				f
			});
		let max = *(freq.values()
			.max()
			.expect("Not empty"));
		let new_label = freq.into_iter()
			.filter(|(_, f)| *f == max)
			.map(|(l, _)| l)
			.choose(&mut rng)
			.expect("There is a max");
		if new_label != labels[v] {
			labels[v] = new_label;
			change = true;
		}
	}
	change
}

#[cfg(test)]
mod tests {
	use super::UGraph;
	use std::fs::File;
	use std::io::BufReader;

	#[test]
	fn greedy_works() {
		let g = UGraph::read_gr(BufReader::new(File::open("test_stuff/test.gr").unwrap())).unwrap();
		let mut g1 = g.clone();
		let k1 = super::greedy(&mut g1);

		assert_eq!(0, g1.num_nodes());
		assert_eq!(0, g1.num_edges());
		assert_eq!(6, k1);

		let mut g2 = g;
		let k2 = super::greedy_rev(&mut g2);

		assert_eq!(0, g2.num_nodes());
		assert_eq!(0, g2.num_edges());
		assert_eq!(31, k2);
	}

	#[test]
	fn greedy_threshold_generalization() {
		let graph = UGraph::read_gr(BufReader::new(File::open("test_stuff/avatar.gr").unwrap())).unwrap();
		_greedy_threshold_generalization(&graph);

		let graph = UGraph::read_gr(BufReader::new(File::open("test_stuff/exact001.gr").unwrap())).unwrap();
		_greedy_threshold_generalization(&graph);

		let graph = UGraph::read_gr(BufReader::new(File::open("test_stuff/test.gr").unwrap())).unwrap();
		_greedy_threshold_generalization(&graph);
	}

	#[test]
	fn two_clusters() {
		let graph = UGraph::read_gr(BufReader::new(File::open("test_stuff/avatar.gr").unwrap())).unwrap();
		assert_eq!(7, super::two_clusters(&graph));

		let graph = UGraph::read_gr(BufReader::new(File::open("test_stuff/test.gr").unwrap())).unwrap();
		assert_eq!(6, super::two_clusters(&graph));
	}

	fn _greedy_threshold_generalization(graph: &UGraph) {
		let mut g = graph.clone();
		let k1 = super::greedy(&mut g);
		let mut g = graph.clone();
		let k2 = super::greedy_w_threshold(&mut g, 0.0);
		assert_eq!(k1 ,k2.0);

		let mut g = graph.clone();
		let k1 = super::greedy_degree(&mut g);
		let mut g = graph.clone();
		let k2 = super::greedy_degree_w_threshold(&mut g, 0.0);
		assert_eq!(k1 ,k2.0);
	}
}
