use std::error;
use std::io;

use kernel_impl::{alt_graph::UGraph, reduce::reduce};

fn main() -> Result<(), Box<dyn error::Error>> {
	let stdin = io::stdin();
	let stdin = stdin.lock();
	let stdout = io::stdout();
	let mut stdout = stdout.lock();
	let graph = UGraph::read_gr(stdin)?;
	let n = graph.num_nodes();
	let m = graph.num_edges();

	eprintln!("Reducing\t({:>4} Nodes, {:>5} Edges)", n, m);
	let (d, kernel) = reduce(graph)?;
	let n = kernel.num_nodes();
	let m = kernel.num_edges();

	eprintln!("SCORE: {}", (n+m+1) as f32/(d+1) as f32);


	println!("{}", d);
	kernel.write_gr(&mut stdout, false)?;

	Ok(())
}
