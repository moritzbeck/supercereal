use std::error;
use std::io;

use kernel_impl::{alt_graph::UGraph, solution::Solution, reduce::reduce};

fn main() -> Result<(), Box<dyn error::Error>> {
	let stdin = io::stdin();
	let stdin = stdin.lock();
	let stdout = io::stdout();
	let mut stdout = stdout.lock();
	let graph = UGraph::read_gr(stdin)?;
	let n = graph.num_nodes();
	let m = graph.num_edges();

	eprintln!("Reducing\t({:>4} Nodes, {:>5} Edges)", n, m);
	let (d, kernel) = reduce(graph.clone())?;
	let n = kernel.num_nodes();
	let m = kernel.num_edges();

	eprintln!("SCORE: {}", (n+m+1) as f32/(d+1) as f32);


	println!("{}", d);
	kernel.write_gr(&mut stdout, false)?;

	eprint!("HEURISTIC 1: ");
	let sp1 = solution_one(&kernel);
	let s1 = sp1.lift_it(&graph, &kernel);
	eprintln!("{} Edits", s1.len());
	println!("{}", s1.len());
	s1.write_solution(&mut stdout)?;

	eprint!("HEURISTIC 2: ");
	let sp2 = solution_two(&kernel);
	let s2 = sp2.lift_it(&graph, &kernel);
	eprintln!("{} Edits", s2.len());
	println!("{}", s2.len());
	s2.write_solution(&mut stdout)?;

	eprint!("HEURISTIC 3: ");
	let sp3 = solution_three(&kernel);
	let s3 = sp3.lift_it(&graph, &kernel);
	eprintln!("{} Edits", s3.len());
	println!("{}", s3.len());
	s3.write_solution(&mut stdout)?;

	eprint!("HEURISTIC 4: ");
	let sp4 = solution_four(&kernel);
	let s4 = sp4.lift_it(&graph, &kernel);
	eprintln!("{} Edits", s4.len());
	println!("{}", s4.len());
	s4.write_solution(&mut stdout)?;

	Ok(())
}

fn solution_one(g: &UGraph) -> Solution {
	g.edges().collect()
}
fn solution_two(g: &UGraph) -> Solution {
	let nodes = g.nodes().collect();
	g.anti_edges_within(&nodes).collect()
}
fn solution_three(g: &UGraph) -> Solution {
	let mut edits = Vec::new();
	let mut g = g.clone();

	let nodes: Vec<_> = g.nodes().collect();
	for i in nodes {
		edits.append(&mut _greedy_helper(&mut g, i));
	}
	Solution::with_edits(edits)
}
fn solution_four(g: &UGraph) -> Solution {
	let mut edits = Vec::new();
	let mut g = g.clone();

	let nodes = {
		let mut nodes: Vec<_> = g.nodes().collect();
		nodes.reverse();
		nodes
	};
	for i in nodes {
		edits.append(&mut _greedy_helper(&mut g, i));
	}
	Solution::with_edits(edits)
}


fn _greedy_helper(g: &mut UGraph, i: usize) -> Vec<(usize, usize)> {
	let mut edits = Vec::new();

	if let Some(mut neighbors) = g.open_neighborhood(i).clone() {
		neighbors.insert(i);

		edits.extend(g.anti_edges_within(&neighbors));

		for &v in neighbors.iter() {
			g.delete_edges_between(v, &neighbors);
			let external_neigbors = g.open_neighborhood(v).as_ref().unwrap().iter().map(|&n| if v < n {(v, n)} else {(n, v)});
			edits.extend(external_neigbors);
			// cannot delete `v` right now, as `delete_edges_between` expects `v` to be there (as part of `neighbors`)
			//g.delete_node(v);
		}
		for v in neighbors {
			g.delete_node(v);
		}
	}

	edits
}
