use std::error;
use std::io::{self, BufRead};

use kernel_impl::{alt_graph::UGraph, cust_error::ImportError, solution::Solution};

fn main() -> Result<(), Box<dyn error::Error>> {
	let stdin = io::stdin();
	let stdin = stdin.lock();
	let stdout = io::stdout();
	let mut stdout = stdout.lock();

	eprintln!("Import...");
	let (graph, d, kernel, sol) = read(stdin)?;
	eprintln!("\tGraph\t({:>4} Nodes, {:>5} Edges)", graph.num_nodes(), graph.num_edges());
	eprintln!("\tKernel\t({:>4} Nodes, {:>5} Edges), d = {}", kernel.num_nodes(), kernel.num_edges(), d);
	eprintln!("\tSolution: {} Edits", sol.len());

	eprintln!();
	eprintln!("Lifting...");
	let full_solution = sol.lift_it(&graph, &kernel);
	eprintln!("\tFull Solution:  {} Edits", full_solution.len());
	eprintln!("\tSolution Bound: {} Edits = {} Edits + {} Edits", sol.len() + d, sol.len(), d);
	full_solution.write_solution(&mut stdout)?;

	Ok(())
}

/// Reads from `r` and returns the original graph `g`, the kernel `k`, the number of edits done for the kernel
/// and the solution `s` to the kernel as a tuple  `(g, d, k, s)`.
fn read<R: BufRead>(r: R) -> Result<(UGraph, usize, UGraph, Solution), Box<dyn error::Error>> {
	let mut parts = r.split(b'#');

	let next_part = parts.next().ok_or(ImportError::Malformed)??;
	let graph = UGraph::read_gr(&next_part[..])?;

	let next_part = parts.next().ok_or(ImportError::Malformed)??;
	let pos = next_part.iter().position(|&c| c == b'\n').ok_or(ImportError::Malformed)?;
	let next_part = &next_part[pos+1..];
	let pos = next_part.iter()
		.position(|&c| c == b'\n')
		.ok_or(ImportError::Malformed)?;
	let d = std::str::from_utf8(&next_part[..pos])?.parse()?;
	let kernel = UGraph::read_gr(&next_part[pos+1..])?;

	let next_part = parts.next().ok_or(ImportError::Malformed)??;
	let solution = Solution::read_solution(&next_part[..])?;

	Ok((graph, d, kernel, solution))
}
