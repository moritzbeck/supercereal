use crate::alt_graph::{UGraph, NodeSet};
use std::collections::HashSet;

use rand::seq::SliceRandom;
use rand::thread_rng;

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct IndependentConflictSet {
	triples: HashSet<(usize, usize, usize)>,
	cycles: HashSet<(usize,usize,usize,usize)>,
	covered_dyads: HashSet<(usize,usize)>,
}

/// A struct maintaining a set of conflict triples.
impl IndependentConflictSet {
	/// Creates an empty conflict set.
	pub fn new() -> Self {
		Default::default()
	}
	/// Returns if the conflict set contains a conflict triple
	/// that has the nodes in `dyad`.
	pub fn covers(&self, dyad: (usize, usize)) -> bool {
		assert!(dyad.0 != dyad.1);
		let dyad = if dyad.0 < dyad.1 { dyad } else { (dyad.1, dyad.0) };
		self.covered_dyads.contains(&dyad)
	}

	/// Add a conflict triple to this conflict set.
	///
	/// If the triple is already [covered](IndependentConflictSet::covers())
	/// the triple won't be added and `false` is returned.
	pub fn add(&mut self, (a, b, c): (usize, usize, usize)) -> bool {
		let mut a = a;
		let mut b = b;
		let mut c = c;
		if a > b {
			std::mem::swap(&mut a, &mut b);
		}
		if b > c {
			std::mem::swap(&mut b, &mut c);
		}
		if a > b {
			std::mem::swap(&mut a, &mut b);
		}

		if self.covers((a,b)) || self.covers((a,c)) || self.covers((b,c)) {
			return false;
		}

		self.triples.insert((a,b,c));
		self.covered_dyads.insert((a,b));
		self.covered_dyads.insert((a,c));
		self.covered_dyads.insert((b,c));
		true
	}

	/// TODO
	pub fn upgrade_to_cycle(&mut self, (a, b, c): (usize, usize, usize), d: usize) -> bool {
		let mut a = a;
		let mut b = b;
		let mut c = c;
		if a > b {
			std::mem::swap(&mut a, &mut b);
		}
		if b > c {
			std::mem::swap(&mut b, &mut c)
		}
		if a > b {
			std::mem::swap(&mut a, &mut b);
		}
		//self.covered_dyads.insert((a,b));
		//self.covered_dyads.insert((a,c));
		//self.covered_dyads.insert((b,c));
		assert!(a < b && b < c);
		assert!(self.triples.contains(&(a,b,c)));
		self.covered_dyads.insert(if a < d {(a,d)} else {(d,a)});
		self.covered_dyads.insert(if b < d {(b,d)} else {(d,b)});
		self.covered_dyads.insert(if c < d {(c,d)} else {(d,c)});
		self.triples.remove(&(a,b,c));
		if d < a {
			self.cycles.insert((d, a, b, c));
		} else if d < b {
			self.cycles.insert((a, d, b, c));
		} else if d < c {
			self.cycles.insert((a, b, d, c));
		} else {
			self.cycles.insert((a, b, c, d));
		}
		// TODO: return value
		true
	}
	pub fn downgrade_from_cycle(&mut self, (a,b,c,d): (usize, usize, usize, usize), del: usize) -> bool {
		assert!(a == del || b == del || c == del || d == del);
		let mut a = a;
		let mut b = b;
		let mut c = c;
		let mut d = d;
		if a > b {
			std::mem::swap(&mut a, &mut b);
		}
		if c > d {
			std::mem::swap(&mut c, &mut d)
		}
		if a > c {
			std::mem::swap(&mut a, &mut c);
		}
		if b > d {
			std::mem::swap(&mut b, &mut d);
		}
		if b > c {
			std::mem::swap(&mut b, &mut c);
		}
		assert!(a < b && b < c && c < d);
		if !self.cycles.contains(&(a, b, c, d)) {
			return false;
		}
		self.cycles.remove(&(a, b, c, d));
		if del == a {
			self.triples.insert((b, c, d));
			self.covered_dyads.remove(&(a,b));
			self.covered_dyads.remove(&(a,c));
			self.covered_dyads.remove(&(a,d));
		} else if del == b {
			self.triples.insert((a, c, d));
			self.covered_dyads.remove(&(a,b));
			self.covered_dyads.remove(&(b,c));
			self.covered_dyads.remove(&(b,d));
		} else if del == c {
			self.triples.insert((a, b, d));
			self.covered_dyads.remove(&(a,c));
			self.covered_dyads.remove(&(b,c));
			self.covered_dyads.remove(&(c,d));
		} else {
			self.triples.insert((a, b, c));
			self.covered_dyads.remove(&(a,d));
			self.covered_dyads.remove(&(b,d));
			self.covered_dyads.remove(&(c,d));
		}
		true
	}
	pub fn search_cycle_covering(&self, (x,y): (usize, usize)) -> Option<(usize, usize, usize, usize)> {
		self.cycles.iter().find(|(a,b,c,d)| {
			(x == *a || x == *b || x == *c || x == *d) &&
			(y == *a || y == *b || y == *c || y == *d)
		}).cloned()
	}

	pub fn bound(&self) -> usize {
		self.triples.len() + 2*self.cycles.len()
	}

	/// Returns the number of conflict triples in this set.
	pub fn _len(&self) -> usize {
		self.triples.len()
	}

	/// Returns if this set is empty (i.e., `self.len() == 0`).
	pub fn is_empty(&self) -> bool {
		self.bound() == 0
	}

	pub fn triples(&self) -> impl Iterator<Item=&(usize, usize, usize)> {
		self.triples.iter()
	}

	/// Removes a conflict triple from this set.
	pub fn remove(&mut self, (a, b, c): (usize, usize, usize)) -> bool {
		let mut a = a;
		let mut b = b;
		let mut c = c;
		if a > b {
			std::mem::swap(&mut a, &mut b);
		}
		if b > c {
			std::mem::swap(&mut b, &mut c)
		}
		if a > b {
			std::mem::swap(&mut a, &mut b);
		}
		if self.triples.remove(&(a,b,c)) {
			self.covered_dyads.remove(&(a,b));
			self.covered_dyads.remove(&(a,c));
			self.covered_dyads.remove(&(b,c));
			true
		} else {
			false
		}
	}

	// pub fn remove_two_match(&mut self, (a,b): (usize, usize), thirds: Vec<usize>) {
	//
	// }
	pub fn local_maximize(&mut self, g: &UGraph) -> usize {
		let mut q = 0;
		let mut changed = true;
		while changed {
			changed = false;
			for triple in &self.triples {
				let mut a = triple.0;
				let mut b = triple.1;
				let mut c = triple.2;
				let mut new_triples = Vec::with_capacity(3);
				let mut forbidden = vec![a, b, c];

				let mut n_a = g.open_neighborhood(a).as_ref().unwrap();
				let mut n_b = g.open_neighborhood(b).as_ref().unwrap();
				let mut n_c = g.open_neighborhood(c).as_ref().unwrap();

				// make (a, c) the non-edge
				if !n_a.contains(&b) {
					std::mem::swap(&mut b, &mut c);
					std::mem::swap(&mut n_b, &mut n_c);
				} else if !n_b.contains(&c) {
					std::mem::swap(&mut a, &mut b);
					std::mem::swap(&mut n_a, &mut n_b);
				}
				assert!(!n_a.contains(&c));

				let candid_1: Vec<_> = n_a.intersection(&n_c).collect();
				let candid_2: Vec<_> = n_a.symmetric_difference(&n_b).collect();
				let candid_3: Vec<_> = n_b.symmetric_difference(&n_c).collect();

				let mut found = Vec::with_capacity(3);

				't: for &c1 in candid_1 {
					if c1 == b { continue; }
					if !self.covers((a, c1)) && !self.covers((c, c1)) {
						forbidden.push(c1);
						new_triples.push((a, c, c1));
					}
					for &&c2 in &candid_2 {
						if forbidden.contains(&c2) { continue; }
						if !self.covers((a, c2)) && !self.covers((b, c2)) {
							forbidden.push(c2);
							new_triples.push((a, b, c2));
						}
						if new_triples.is_empty() { break; }
						for &&c3 in &candid_3 {
							if forbidden.contains(&c3) { continue; }
							if !self.covers((b, c3)) && !self.covers((c, c3)) {
								forbidden.push(c3);
								new_triples.push((b, c, c3));
								if new_triples.len() == 3 {
									std::mem::swap(&mut found, &mut new_triples);
									break 't;
								} else {
									if new_triples.len() == 2 {
										found = new_triples.clone();
									}
									forbidden.pop();
									new_triples.pop();
								}
							}
						}
						if forbidden[forbidden.len()-1] == c2 {
							forbidden.pop();
							new_triples.pop();
						}
					}
					if forbidden[forbidden.len()-1] == c1 {
						forbidden.pop();
						new_triples.pop();
					}
				}

				if found.len() > 1 {
					q += found.len() - 1;
					self.remove((a, b, c));
					for t in found {
						self.add(t);
					}
					changed = true;
					break;
				}
			}
		}
		//eprintln!("IS +{}", q);
		q
	}
}


impl UGraph {
	/*
	/// Don't use this! The graphs get *huge*.
	/// Returns a conflict graph of conflict triples.
	///
	/// The conflict graph has a node for each conflict triple (induced P_3)
	/// and an edge between conflicting triples.
	///
	/// Two conflict triples are said to conflict with each other
	/// iff they share a pair of nodes (i.e. edge or anti-edge).
	#[deprecated]
	fn build_p3_graph(&self) -> NamedGraph<TripNode>{
		let mut num_nodes: usize = 0;
		let mut anti_to_node: HashMap<(usize,usize), Vec<TripNode>> = HashMap::new();
		let mut connected_triples: Vec<TripNode> = Vec::new();
		let mut adj_list: Vec<Option<HashSet<usize>>> = Vec::new();
		let mut names: Vec<TripNode> = Vec::new();

		let g = self.clone();
		for (src, trg) in g.edges() {
			let nsrc = g.open_neighborhood(src).as_ref().unwrap();
			let ntrg = g.open_neighborhood(trg).as_ref().unwrap();
			// All conflict triples containing src and trg are found by symmetric difference.
			//let nthird: HashSet<usize> = nsrc.symmetric_difference(ntrg).collect();

			// we know that `src < trg` holds
			for node in nsrc.difference(ntrg) {
				if *node == trg {
					continue
				}
				// Anti edge is (node, trg)
				let anti_edge = if *node < trg {(*node,trg)} else {(trg,*node)};

				// Triples are named by anti edge
				// Check if triple exists
				let entry = anti_to_node.entry(anti_edge).or_default();
				if let Some(trip_node) = entry.iter().find(|item| {
					item.a == anti_edge.0 && item.b == src &&item.c == anti_edge.1
				}) {
					connected_triples.push(*trip_node);

				} else {
					// If not connect triple with all triples with the same name
					let new_node = TripNode{a: anti_edge.0, b: src, c: anti_edge.1, id: num_nodes};
					num_nodes += 1;
					names.push(new_node);
					adj_list.push(Some(HashSet::new()));
					for w in entry.iter() {
						adj_list[w.id].as_mut().unwrap().insert(new_node.id);
						adj_list[new_node.id].as_mut().unwrap().insert(w.id);
					}
					connected_triples.push(new_node);
					entry.push(new_node);
				}
			}
			for node in ntrg.difference(nsrc) {
				if *node == src {
					continue
				}
				// Anti edge is (node, trg)
				let anti_edge = if *node < src { (*node, src) } else { (src, *node) };

				// Triples are named by anti edge
				// Check if triple exists
				let entry = anti_to_node.entry(anti_edge).or_default();
				if let Some(trip_node) = entry.iter().find(|item| {
					item.a == anti_edge.0 && item.b == trg && item.c == anti_edge.1
				}) {
					connected_triples.push(*trip_node);
				} else {
					// If not connect triple with all triples with the same name
					let new_node = TripNode { a: anti_edge.0, b: trg, c: anti_edge.1, id: num_nodes };
					num_nodes += 1;
					names.push(new_node);
					adj_list.push(Some(HashSet::new()));
					for w in entry.iter() {
						adj_list[w.id].as_mut().unwrap().insert(new_node.id);
						adj_list[new_node.id].as_mut().unwrap().insert(w.id);
					}
					connected_triples.push(new_node);
					entry.push(new_node);
				}
			}

			// Connect all found triples
			for a in &connected_triples {
				for b in &connected_triples {
					if a.id < b.id {
						adj_list[a.id].as_mut().unwrap().insert(b.id);
						adj_list[b.id].as_mut().unwrap().insert(a.id);
					}
				}
			}
			connected_triples.clear();
		}
		NamedGraph {
			names,
			graph: UGraph {
				adj_list,
                ..Default::default()
			}
		}
	}
	*/

	/// Returns a struct maintaining a set of pair-disjoint conflict triples.
	pub fn find_independent_conflict_set(&self) -> IndependentConflictSet {
		let mut rng = thread_rng();
		let mut indie_set = IndependentConflictSet::new();
		let edges = {
			let mut e: Vec<_> = self.edges().collect();
			e.shuffle(&mut rng);
			e
		};
		for (src, trg) in edges {

			// Check if (src, trg) is covered
			if indie_set.covers((src, trg)) {
				continue;
			}
			let nsrc = self.open_neighborhood(src).as_ref().unwrap();
			let ntrg = self.open_neighborhood(trg).as_ref().unwrap();
			// we know that `src < trg` holds
			for node in nsrc.symmetric_difference(ntrg) {
				if *node == trg || *node == src {
					continue
				}
				// in this case id is irrelevant
				if indie_set.add((src,trg,*node)) {
					break;
				}
			}
		}
		indie_set
	}

	/// Returns a struct maintaining a set of pair-disjoint conflict triples.
	pub fn find_independent_conflict_set_ordered(&self) -> IndependentConflictSet {
		let mut indie_set = IndependentConflictSet::new();
		for (src, trg) in self.edges() {

			// Check if (src, trg) is covered
			if indie_set.covers((src, trg)) {
				continue;
			}
			let nsrc = self.open_neighborhood(src).as_ref().unwrap();
			let ntrg = self.open_neighborhood(trg).as_ref().unwrap();
			// we know that `src < trg` holds
			for node in nsrc.symmetric_difference(ntrg) {
				if *node == trg || *node == src {
					continue
				}
				// in this case id is irrelevant
				if indie_set.add((src,trg,*node)) {
					break;
				}
			}
		}
		indie_set
	}

	pub fn find_conflict_cycles(&self, set: &mut IndependentConflictSet) {
		//let mut cyc = 0;
		for (a, b, c) in set.triples().cloned().collect::<Vec<_>>() {
			// determine which dyad is the non-edge
			// and rename the nodes such that (a, c) is the non-edge
			let neighborhood = self.open_neighborhood(a).as_ref().unwrap();
			let (a, b, c) = if !neighborhood.contains(&b) {
				(a, c, b)
			} else if !neighborhood.contains(&c) {
				(a, b, c)
			} else {
				(b, a, c)
			};

			let n_a = self.open_neighborhood(a).as_ref().unwrap();
			let n_b = self.open_neighborhood(b).as_ref().unwrap();
			let n_c = self.open_neighborhood(c).as_ref().unwrap();
			for &candidate in n_a.intersection(n_c) {
				if candidate == b {
					continue;
				}
				if !n_b.contains(&candidate) &&
					!set.covers((a, candidate)) &&
					!set.covers((b, candidate)) &&
					!set.covers((c, candidate)) {
					set.upgrade_to_cycle((a,b,c), candidate);
					//cyc += 1;
					break;
				}
			}
		}
		//eprintln!("Cycles: {}", cyc);
	}

	pub fn do_the_k_plus_one(&mut self, (src, trg): (usize, usize), indie_sets: &mut IndependentConflictSet, k: usize) -> usize {
		assert_ne!(src, trg);
		let nsrc = self.open_neighborhood(src).as_ref().unwrap();
		let ntrg = self.open_neighborhood(trg).as_ref().unwrap();
		let is_edge = nsrc.contains(&trg);

		let thirds: NodeSet = if is_edge {
			nsrc.symmetric_difference(ntrg).filter(|&&node| node != trg && node != src).copied().collect()
		} else {
			nsrc.intersection(ntrg).copied().collect()
		};
		let l = indie_sets.triples.iter().filter(|(a,b,c)| {
			let mut i = 0;
			if thirds.contains(a) || thirds.contains(b) || thirds.contains(c) {
				i += 1;
			}
			if *a == src || *b == src || *c == src {
				i += 1;
			}
			if *a == trg || *b == trg || *c == trg {
				i += 1;
			}
			i < 2
		}).count() + indie_sets.cycles.iter().map(|(a,b,c,d)| {
			let mut i = 0;
			if *a == src || *a == trg || thirds.contains(a) {
				i += 1;
			}
			if *b == src || *b == trg || thirds.contains(b) {
				i += 1;
			}
			if *c == src || *c == trg || thirds.contains(c) {
				i += 1;
			}
			if *d == src || *d == trg || thirds.contains(d) {
				i += 1;
			}
			if i < 2 {
				2
			} else if i == 2 {
				1
			} else {
				0
			}
		}).sum::<usize>();

		if thirds.len() > k-l {
			// edit (src, trg)
			if is_edge {
				self.delete_edge((src, trg));
			} else {
				self.insert_edge((src, trg));
			}
			self.record_flip((src, trg));
			for node in thirds {
				// removes at most one triple/downgrades at most one cycle
				if indie_sets.remove((src, trg, node)) {
					break;
				}
			}

			if let Some(cycle) = indie_sets.search_cycle_covering((src, trg)) {
				indie_sets.downgrade_from_cycle(cycle, src);
			}

			let nsrc = self.open_neighborhood(src).as_ref().unwrap();
			let ntrg = self.open_neighborhood(trg).as_ref().unwrap();

			// update indie_sets to include newly created conflict triples
			if !is_edge {
				// (src, trg) was a non-edge, now we've flipped it
				let thirds: Vec<_> = nsrc.symmetric_difference(ntrg)
					.filter(|&&node| node != trg && node != src
						&& !indie_sets.covers((src, node)) && !indie_sets.covers((trg, node)))
					.collect();
				let mut found_cycle = false;
				'add0: for node1 in &thirds {
					for node2 in &thirds {
						if node1 == node2 { continue; }
						if !indie_sets.covers((**node1, **node2)) &&
							(nsrc.contains(*node1) ^ nsrc.contains(*node2)) &&
							self.open_neighborhood(**node1).as_ref().unwrap().contains(*node2) {
							found_cycle = true;
							if !indie_sets.add((src, trg, **node1)) {
								eprintln!("0 Didn't add ({}, {}, {})", src, trg, **node1);
							}
							indie_sets.upgrade_to_cycle((src, trg, **node1), **node2);
							break 'add0;
						}
					}
				}
				if !found_cycle {
					if !thirds.is_empty() {
						indie_sets.add((src, trg, *thirds[0]));
					}
				}
			} else {
				// (src, trg) was an edge, now we've flipped it
				let thirds: Vec<_> = nsrc.intersection(ntrg)
					.filter(|&&node| !indie_sets.covers((src, node)) && !indie_sets.covers((trg, node)))
					.collect();
				let mut found_cycle = false;
				'add1: for node1 in &thirds {
					for node2 in &thirds {
						if node1 == node2 { continue; }
						if !indie_sets.covers((**node1, **node2)) &&
							!self.open_neighborhood(**node1).as_ref().unwrap().contains(*node2) {
							found_cycle = true;
							if !indie_sets.add((src, trg, **node1)) {
								eprintln!("1 Didn't add ({}, {}, {})", src, trg, **node1);
							}
							indie_sets.upgrade_to_cycle((src, trg, **node1), **node2);
							break 'add1;
						}
					}
				}
				if !found_cycle {
					if !thirds.is_empty() {
						indie_sets.add((src, trg, *thirds[0]));
					}
				}
			};


			0
		} else {
			k-l - thirds.len() + 1
		}
	}
}

#[cfg(test)]
mod test {
	use crate::alt_graph::UGraph;
	use std::io::Cursor;

	/*
	#[test]
	fn build_p3_simple() {
		let gr = Cursor::new("p cep 4 4\n1 2\n2 3\n3 4\n4 1\n");
		let graph = UGraph::read_gr(gr).unwrap();
		let p3_graph = graph.build_p3_graph();

		assert_eq!(4, p3_graph.graph.num_nodes());
		assert_eq!(4, p3_graph.names.len());
		assert_eq!(6, p3_graph.graph.num_edges());
	}

	#[test]
	fn built_p3_larger() {
		let gr = Cursor::new("p cep 10 11\n1 2\n2 3\n3 4\n4 5\n5 6\n5 7\n5 8\n7 8\n7 9\n8 10\n9 10\n");
		let graph = UGraph::read_gr(gr).unwrap();
		let p3_graph = graph.build_p3_graph();

		assert_eq!(14, p3_graph.names.len());
		assert_eq!(14, p3_graph.graph.num_nodes());
		assert_eq!(27, p3_graph.graph.num_edges());
		let n012 = p3_graph.names.iter().find(|n| n.a == 0 && n.b == 1 && n.c == 2).unwrap().id;
		assert_eq!(1, p3_graph.graph.degree(n012));
		let n234 = p3_graph.names.iter().find(|n| n.a == 2 && n.b == 3 && n.c == 4).unwrap().id;
		assert_eq!(4, p3_graph.graph.degree(n234));
		let n347 = p3_graph.names.iter().find(|n| n.a == 3 && n.b == 4 && n.c == 7).unwrap().id;
		assert_eq!(5, p3_graph.graph.degree(n347));
		let n547 = p3_graph.names.iter().find(|n| n.a == 5 && n.b == 4 && n.c == 7).unwrap().id;
		assert_eq!(4, p3_graph.graph.degree(n547));
		assert!(p3_graph.names.iter().find(|n| n.a == 4 && n.b == 5 && n.c == 7).is_none());
	}

	#[test]
	fn conflict_graph_greedy_independent_set() {
		let gr = Cursor::new("p cep 4 4\n1 2\n2 3\n3 4\n4 1\n");
		let graph = UGraph::read_gr(gr).unwrap();
		let p3_graph = graph.build_p3_graph();
		let is = p3_graph.greedy_independent_set();
		assert_eq!(1, is.len());
		let gr = Cursor::new("p cep 10 11\n1 2\n2 3\n3 4\n4 5\n5 6\n5 7\n5 8\n7 8\n7 9\n8 10\n9 10\n");
		let graph = UGraph::read_gr(gr).unwrap();
		let p3_graph = graph.build_p3_graph();
		let is = p3_graph.greedy_independent_set();
		assert_eq!(5, is.len());
	}
	*/

	#[test]
	fn greedy_independent_set() {
		let gr = Cursor::new("p cep 4 4\n1 2\n2 3\n3 4\n4 1\n");
		let graph = UGraph::read_gr(gr).unwrap();
		let is = graph.find_independent_conflict_set();
		assert_eq!(1, is.len());
		let gr = Cursor::new("p cep 10 11\n1 2\n2 3\n3 4\n4 5\n5 6\n5 7\n5 8\n7 8\n7 9\n8 10\n9 10\n");
		let graph = UGraph::read_gr(gr).unwrap();
		let is = graph.find_independent_conflict_set_ordered();
		assert_eq!(5, is.len());
	}

	#[test]
	fn k_plus_one_rule(){
		let gr = Cursor::new("p cep 7 10\n1 3\n1 4\n1 5\n1 6\n1 7\n2 3\n2 4\n2 5\n2 6\n2 7\n");
		let mut graph = UGraph::read_gr(gr).unwrap();
		let mut is = graph.find_independent_conflict_set();
		dbg!(&is);
		graph.do_the_k_plus_one((0,1), &mut is, 5);
		assert_eq!(graph.num_edges(), 10);
		graph.do_the_k_plus_one((0,1), &mut is, 4);
		assert_eq!(graph.num_edges(), 11);
		dbg!(is);
	}

}
