use crate::alt_graph::UGraph;

impl UGraph {

    pub fn complete_p3_reduction(&mut self) -> usize {
		let mut edits = 0;
        // Clone the graph to avoid borrowing errors
        let g = self.clone();
        for (src, trg) in g.edges() {
            let nsrc = g.open_neighborhood(src).as_ref().unwrap();
            let ntrg = g.open_neighborhood(trg).as_ref().unwrap();
            // we know that `src < trg` holds
            for node in nsrc.difference(ntrg) {
                // node connected to src
                // we demand `node > trg` to consider every conflict triple exactly once
                if *node > trg {
                    let nsrc = self.open_neighborhood(src).as_ref().unwrap();
                    let ntrg = self.open_neighborhood(trg).as_ref().unwrap();
                    // As we don't update `g` when edges of `self` are modified,
                    // we have to check if there still is an edge between `src` and `trg`.
                    if !nsrc.contains(&trg) { break; }
                    // we have to check if there still is an edge between `src` and `node`.
                    if !nsrc.contains(&node) { continue; }
                    // we have to check if there still is no edge between `trg` and `node`.
                    if ntrg.contains(&node) { continue; }
                    let conflict = [trg, src, *node];
                    if self.p3_reduction(conflict) {
						edits += 1;
					}
                }
            }
            for node in ntrg.difference(nsrc) {
                // node connected to trg
                // we demand `node > src` to consider every conflict triple exactly once
                if *node > src {
                    let nsrc = self.open_neighborhood(src).as_ref().unwrap();
                    let ntrg = self.open_neighborhood(trg).as_ref().unwrap();
                    // As we don't update `g` when edges of `self` are modified,
                    // we have to check if there still is an edge between `src` and `trg`.
                    if !nsrc.contains(&trg) { break; }
                    // we have to check if there still is an edge between `trg` and `node`.
                    if !ntrg.contains(&node) { continue; }
                    // we have to check if there still is no edge between `src` and `node`.
                    if nsrc.contains(&node) { continue; }
                    let conflict = [src, trg, *node];
                    if self.p3_reduction(conflict) {
						edits += 1;
					}
                }
            }
        }

		edits
    }

    /// Reduce P_3: (u,v), (v,w)
    fn p3_reduction(&mut self, [u,v,w]: [usize; 3]) -> bool {
        let mut n_u = self.open_neighborhood(u).as_ref().cloned().unwrap();
        let mut n_v = self.open_neighborhood(v).as_ref().cloned().unwrap();
        let mut n_w = self.open_neighborhood(w).as_ref().cloned().unwrap();

        // Add W to all neighbourhoods
        n_u.extend([u,v,w].iter());
        n_v.extend([u,v,w].iter());
        n_w.extend([u,v,w].iter());

        let nu_eq_nv = n_u.eq(&n_v);
        let nv_eq_nw = n_v.eq(&n_w);

        // Cases:
        // (u,v)
        if nu_eq_nv && n_u.intersection(&n_w).count() == 3 {
            // The neighborhoods all contain u, v and w; hence 3 instead of 0.
            self.delete_edge((v,w));
            self.record_flip((v,w));
            true
        } else if nv_eq_nw && n_v.intersection(&n_u).count() == 3 { // (v,w)
            // The neighborhoods all contain u, v and w; hence 3 instead of 0.
            self.delete_edge((u,v));
            self.record_flip((u,v));
            true
        } else if nu_eq_nv && nv_eq_nw { // (u,v), (v,w) and (w,u)
            self.insert_edge((w,u));
            self.record_flip((w,u));
            true
        } else {
            false
        }
    }
}


#[cfg(test)]
mod test {
    use crate::alt_graph::UGraph;
    use std::io::Cursor;

    #[test]
    fn reduction() {
        //let mut graph = UGraph::import_gr("test_stuff/p3_free_001.gr").unwrap();
        let gr = Cursor::new("p cep 5 9\n1 2\n1 3\n1 4\n1 5\n2 3\n2 4\n2 5\n3 5\n4 5\n");
        let mut graph = UGraph::read_gr(gr).unwrap();
        graph.complete_p3_reduction();
        dbg!(&graph);
        assert_eq!(10, graph.edges().count());

        let gr = Cursor::new("p cep 5 7\n1 2\n\n1 3\n1 4\n2 3\n2 4\n3 4\n4 5");
        let mut graph = UGraph::read_gr(gr).unwrap();
        graph.complete_p3_reduction();
        dbg!(&graph);
        assert_eq!(6, graph.edges().count());

        let gr = Cursor::new("p cep 5 6\n1 2\n1 3\n2 3\n3 4\n3 5\n4 5\n");
        let mut graph = UGraph::read_gr(gr).unwrap();
        let old = graph.clone();
        graph.complete_p3_reduction();
        assert_eq!(graph, old);

        let gr = Cursor::new("p cep 5 5\n1 2\n1 3\n2 3\n3 4\n3 5\n\n");
        let mut graph = UGraph::read_gr(gr).unwrap();
        let old = graph.clone();
        graph.complete_p3_reduction();
        assert_eq!(graph, old);
    }
}
