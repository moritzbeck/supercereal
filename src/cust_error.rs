use core::fmt;
use std::error::Error;

#[derive(Debug)]
pub enum ImportError {
    Io(std::io::Error),
    Malformed,
    BadInt(std::num::ParseIntError),
}

impl From<std::io::Error> for ImportError {
    fn from(e: std::io::Error) -> ImportError {
        ImportError::Io(e)
    }
}

impl From<std::num::ParseIntError> for ImportError {
    fn from(e: std::num::ParseIntError) -> ImportError {
        ImportError::BadInt(e)
    }
}

impl fmt::Display for ImportError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Io(_) => write!(f, "Import: IoError"),
            Self::Malformed => write!(f, "Import: Input is malformed."),
            Self::BadInt(_) => write!(f, "Import: Integer is malformed."),
        }
    }
}

impl Error for ImportError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            Self::Io(e) => Some(e),
            Self::Malformed => None,
            Self::BadInt(e) => Some(e),
        }
    }
}