use crate::alt_graph::UGraph;
use std::collections::{HashMap, HashSet};



impl UGraph {
	/// Returns index of a reducible node if existent.
	pub fn find_reducible(&self) -> Option<usize> {
		self.nodes().find(|i| self.is_reducible(*i))
	}

	/// Reduces ...
	///
	/// If `node` is not reducible (as determined by `is_reducible`) this function does not work properly.
	pub fn reduce(&mut self, node: usize) -> usize {
		let mut edits = self.stable_cost(node).1;
		debug_assert!(self.is_reducible(node));
		let mut clique = self.open_neighborhood(node).as_ref().unwrap().clone();
		clique.insert(node); // closed neighborhood (will be a clique if `node` reducible)

		let neighborhood = self.open_neighborhood_set(clique.iter().cloned()); // open neighborhood of clique
		let size = clique.len();

		let mut smaller_clique = HashSet::new();
		let mut da_real_x = None;
		for x in neighborhood {
			if !smaller_clique.is_empty() {
				// delete
				self.delete_edges_between(x, &smaller_clique); // Since `clique`\`smaller_clique` is already marked for deletion.
			} else {
				let adj: Vec<_> = self.adjacent_in(x, &clique).collect();
				if adj.len() <= size/2 {
					// delete
					self.delete_edges_between(x, &clique);
				} else {
					let www = 2*adj.len() - size; // = e - a
					edits -= www;
					smaller_clique = adj.into_iter().take(www).collect();
					da_real_x = Some(x);
				}
			}
		}
		if smaller_clique.is_empty() {
			// delete isolated clique
			self.delete_component(&clique);
			// Solutionlifting: one clique
			self.record_clique(&clique);
		} else {
			// delete nodes in `clique` \ `smaller_clique` (rule 3)
			let clique_difference: HashSet<usize> = clique.difference(&smaller_clique).copied().collect();
			for v in &clique_difference {
				self.delete_node(*v);
			}
			// Solutionlifting: `clique` \ `smaller_clique` belongs to `smaller_clique` (they are part of the same cluster).
			if let Some(x) = da_real_x {
				self.record_belonging(&clique_difference, &smaller_clique, x);
			} else {
				panic!("No real x")
			}
			// make `smaller_clique` actually be a clique (rule 1)
			let anti_edges: Vec<_> = self.anti_edges_within(&smaller_clique).collect();
			self.insert_edges(anti_edges);
		}

		edits
	}

	pub fn critical_cliques(&self) -> CriticalCliques {
		CriticalCliques::from_graph(self)
	}
}

#[derive(Debug)]
pub struct CriticalCliques {
	clique_indices: Vec<usize>,
	cliques: Vec<Vec<usize>>,
}

impl CriticalCliques {
	pub fn from_graph(g: &UGraph) -> Self {
		let mut neighbours_to_nodes = HashMap::new();

		for node in g.nodes() {
			let mut neigh: Vec<_> = g.open_neighborhood(node).as_ref().unwrap().iter().copied().collect();
			neigh.push(node);
			neigh.sort_unstable();

			neighbours_to_nodes.entry(neigh).or_insert_with(Vec::new).push(node);
		}

		let mut idx = 0;
		let mut clique_indices = vec![0; g.adj_list_len()];
		let mut cliques = Vec::new();
		for (_, clique) in neighbours_to_nodes {
			for &node in clique.iter() {
				clique_indices[node] = idx;
			}
			cliques.push(clique);
			idx += 1;
		}
		CriticalCliques {
			clique_indices,
			cliques
		}
	}

	// TODO: Is it okay that this assigns deleted nodes the cluster_id 0?

	pub fn clique_id(&self, node: usize) -> usize {
		self.clique_indices[node]
	}
	pub fn same_clique(&self, node1: usize, node2: usize) -> bool {
		self.clique_indices[node1] == self.clique_indices[node2]
	}
	pub fn nodes(&self, clique_id: usize) -> &[usize] {
		&self.cliques[clique_id]
	}
}


#[cfg(test)]
mod tests {
	use crate::alt_graph::UGraph;
	use super::CriticalCliques;
	use std::io::BufReader;
	use std::fs::File;

	#[test]
	fn edge_cut_and_back() {
		let mut graph = UGraph::read_gr(BufReader::new(File::open("test_stuff/test.gr").unwrap())).unwrap();
		let org_graph = graph.clone();
		while let Some(reducible) = graph.find_reducible() {
            graph.reduce(reducible);
		}
        while graph.reverse_last_edit() {}
		assert_eq!(graph.adj_list, org_graph.adj_list);
	}

	#[test]
	fn critical_clique_indices() {
		let graph = UGraph::read_gr(BufReader::new(File::open("test_stuff/test.gr").unwrap())).unwrap();

		let cc = CriticalCliques::from_graph(&graph);
		assert!(cc.same_clique(0, 2));
		assert!(cc.same_clique(0, 3));
		assert!(cc.same_clique(2, 3));

		assert!(cc.same_clique(1, 5));
		assert!(cc.same_clique(1, 8));
		assert!(cc.same_clique(1, 9));
		assert!(cc.same_clique(5, 8));
		assert!(cc.same_clique(5, 9));
		assert!(cc.same_clique(8, 9));

		assert!(!cc.same_clique(0, 1));

		for &i in &[4, 6, 7, 10, 11] {
			for j in 0..12 {
				if i == j { continue; }
				eprintln!("({}, {})", i, j);
				assert!(!cc.same_clique(i, j));
			}
		}
	}
	#[test]
	fn critical_cliques() {
		let graph = UGraph::read_gr(BufReader::new(File::open("test_stuff/test.gr").unwrap())).unwrap();

		let cc = CriticalCliques::from_graph(&graph);

		let cliques = vec![( 0, vec![0,2,3]),
			               ( 1, vec![1,5,8,9]),
			               ( 4, vec![4]),
			               ( 6, vec![6]),
			               ( 7, vec![7]),
			               (10, vec![10]),
			               (11, vec![11])];

		for (i, c) in cliques {
			let id = cc.clique_id(i);
			assert_eq!(c, cc.nodes(id));
		}
	}
}
