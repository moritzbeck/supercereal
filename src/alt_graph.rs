use std::io::{self, prelude::*};
use std::collections::HashSet;
use std::iter::FromIterator;

use crate::cust_error::ImportError;
use crate::solution::Solution;

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct UGraph {
    /// The adjacency List
    pub(crate) adj_list: Vec<Option<HashSet<usize>>>,
    pub(crate) edits: Vec<Edits>,
    pub(crate) records: Vec<SolutionLifting>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Edits {
    DeletedNode(usize, HashSet<usize>),
    DeletedEdge((usize, usize)),
    InsertedNode(usize),
    InsertedEdge((usize, usize)),
    DeletedComp(Vec<(usize, HashSet<usize>)>)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum SolutionLifting {
    // Insert & Delete Edge in k+1 and p3free will be recorded as simple comment #e src trg
    FlipEdge((usize, usize)), // format: c e src trg
    MakeClique(HashSet<usize>), // format: c c node1 node2 ...
    BelongsTo(HashSet<usize>, HashSet<usize>, usize), // format: c b node1 node2 ... t node1 node2 ... x node
}

impl UGraph {

    pub fn record_flip(&mut self, edge: (usize, usize)) {
        self.records.push(SolutionLifting::FlipEdge(edge));
    }

    pub fn record_clique(&mut self, cluster: &HashSet<usize>) {
        self.records.push(SolutionLifting::MakeClique(cluster.clone()));
    }

    pub fn record_belonging(&mut self, belongs: &HashSet<usize>, to: &HashSet<usize>, x: usize) {
        self.records.push(SolutionLifting::BelongsTo(belongs.clone(), to.clone(), x));
    }

    pub fn reverse_last_edit(&mut self) -> bool {
        match self.edits.pop() {
            None => return false,
            Some(edit) => {
                match edit {
                    Edits::DeletedNode(node, adj) => {
                        for v in &adj {
                            self.adj_list[*v].as_mut().unwrap().insert(node);
                        }
                        self.adj_list[node] = Some(adj);
                    },
                    Edits::DeletedEdge(edge) => {
                        self.adj_list[edge.0].as_mut().unwrap().insert(edge.1);
                        self.adj_list[edge.1].as_mut().unwrap().insert(edge.0);
                    },
                    Edits::InsertedNode(node) => {
                        for v in self.adj_list[node].take().unwrap() {
                            self.adj_list[v].as_mut().unwrap().remove(&node);
                        }
                    },
                    Edits::InsertedEdge(edge) => {
                        self.adj_list[edge.0].as_mut().unwrap().remove(&edge.1);
                        self.adj_list[edge.1].as_mut().unwrap().remove(&edge.0);
                    },
                    Edits::DeletedComp(nodes) => {
                        for (node, adj) in nodes {
                            self.adj_list[node] = Some(adj);
                        }
                    },
                }
            }
        }
        true
    }

    /// Returns if there is a node with the given ID in the graph.
    pub fn has_node(&self, node: usize) -> bool {
        node < self.adj_list.len() && self.adj_list[node].is_some()
    }

    /// Returns the open neighborhood of `node` i.e., the neighbors of `node` but not `node` itself.
    ///
    /// Returns `None`, if `node` is not a valid index of a (not deleted) node.
    pub fn open_neighborhood(&self, node: usize) -> &Option<HashSet<usize>> {
        &self.adj_list[node]
    }

    /// Returns the open neighbourhood of a collection of nodes i.e.,
    /// the nodes adjacent to `nodes` but not in `nodes` themselves.
    ///
    /// # Examples
    /// ```ignore
    ///    let set = graph.open_neighborhood(0)
    ///        .as_ref().unwrap()
    ///        .iter()
    ///        .copied();
    ///    graph.open_neighborhood_set(set);
    /// ```
    pub fn open_neighborhood_set<I>(&self, nodes: I) -> HashSet<usize>
        where I: IntoIterator<Item=usize>,
              I::IntoIter: Clone
    {
        let mut neighbors = HashSet::new();

        let it = nodes.into_iter();

        for n in it.clone() {
            let empty = HashSet::new();
            let node_neigh = self.adj_list[n].as_ref().unwrap_or(&empty);
            neighbors.extend(node_neigh);
        }
        let set = it.collect();

        neighbors.difference(&set).copied().collect()
    }
    /// Deletes a node from the graph.
    pub fn delete_node(&mut self, node: usize) {
        if let Some(neighbors) = &self.adj_list[node].take() {
            for neighbor in neighbors {
                self.adj_list[*neighbor].as_mut().unwrap().remove(&node);
            }
            self.edits.push(Edits::DeletedNode(node, neighbors.clone()))
        }
    }
    /// Delete nodes from the graph.
    ///
    /// # Panics
    /// Panics if there is a node index that is not a valid index of a (not deleted) node.
    pub fn delete_nodes<I: IntoIterator<Item=usize>>(&mut self, nodes: I) {
        for node in nodes {
            self.delete_node(node)
        }
    }

    /// Returns an iterator over all nodes.
    pub fn nodes(&self) -> impl Iterator<Item=usize> + '_ {
        self.adj_list.iter()
            .enumerate()
            .filter_map(|(index, node)| {
                if node.is_some() {
                    Some(index)
                } else {
                    None
                }
            })
    }

    /// Returns the length of the adjacency list.
    pub(crate) fn adj_list_len(&self) -> usize {
        self.adj_list.len()
    }

    /// Returns the number of nodes in the graph.
    pub fn num_nodes(&self) -> usize {
        self.nodes().count()
    }
    /// Returns the number of edges of the graph.
    ///
    /// This is the number of undirected edges.
    /// It is half the length of the iterator returned by [`self.edges()`](`Self::edges()`).
    pub fn num_edges(&self) -> usize {
        self.adj_list.iter()
            .filter_map(|neighbors| {
                if let Some(edges) = neighbors {
                    Some(edges.len())
                } else {
                    None
                }
            }).sum::<usize>() / 2
    }
    /// Returns the maximum degree in this graph.
    ///
    /// Returns 0 if the graph is empty.
    pub fn max_degree(&self) -> usize {
        self.adj_list.iter()
            .map(|neighbors| {
                if let Some(edges) = neighbors {
                    edges.len()
                } else {
                    0
                }
            })
            .max()
            .unwrap_or(0)
    }
    /// Inserts a pair of nodes as an edge into the graph.
    /// Returns if the edge was absent.
    ///
    /// # Panics
    /// Panics if there is a node index that is not a valid index of a (not deleted) node.
    pub fn insert_edge(&mut self, edge: (usize, usize)) -> bool {
        let r1 = self.adj_list[edge.0].as_mut().unwrap().insert(edge.1);
        let r2 = self.adj_list[edge.1].as_mut().unwrap().insert(edge.0);
        self.edits.push(Edits::InsertedEdge((edge.0, edge.1)));
        debug_assert_eq!(r1, r2);
        r2
    }
    /// Inserts pairs of nodes as edges into the graph.
    ///
    /// # Panics
    /// Panics if there is a node index that is not a valid index of a (not deleted) node.
    pub fn insert_edges<I: IntoIterator<Item=(usize, usize)>>(&mut self, edges: I) {
        for edge in edges {
            self.adj_list[edge.0].as_mut().unwrap().insert(edge.1);
            self.adj_list[edge.1].as_mut().unwrap().insert(edge.0);
            self.edits.push(Edits::InsertedEdge((edge.0, edge.1)));
        }
    }
    /// Removes the edge between `src` and `trg`.
    /// Returns if the edge was present.
    ///
    /// Does nothing when the edge does not exist.
    ///
    /// # Panics
    /// Panics if there is a node index that is not a valid index of a (not deleted) node.
    pub fn delete_edge(&mut self, (src, trg): (usize, usize)) -> bool {
        let r1 = self.adj_list[src].as_mut().unwrap().remove(&trg);
        let r2 = self.adj_list[trg].as_mut().unwrap().remove(&src);
        self.edits.push(Edits::DeletedEdge((src, trg)));
        debug_assert_eq!(r1, r2);
        r2
    }
    /// Returns an iterator over all edges.
    ///
    /// Each undirected edge `(src, trg)` is ordered such that `src` < `trg`.
    pub fn edges(&self) -> impl Iterator<Item=(usize, usize)> + '_ {
        self.adj_list.iter()
            .enumerate()
            .filter(|(_, neighbors)| neighbors.is_some())
            .flat_map(|(node, neighbors)| {
                let targets = neighbors.as_ref().unwrap();
                targets.iter().filter_map(move |&trg| {
                    if node < trg {
                        Some((node, trg))
                    } else {
                        None
                    }
                })
            })
    }

    /// Returns the degree of `node`.
    pub fn degree(&self, node: usize) -> usize {
        self.adj_list[node]
            .as_ref()
            .map(|neighbors| neighbors.len())
            .unwrap_or(0)
    }

    /// Returns an iterator over node ids, sorted by degree.
    pub fn nodes_by_degree<'a: 'c, 'c>(&'a self) -> impl Iterator<Item=usize> + 'c {
        let mut adjs: Vec<_> = self.adj_list.iter()
            .cloned()
            .enumerate()
            .filter_map(|(i, adj)| {
                if let Some(list) = adj {
                    Some((i, list))
                } else {None}
            })
            .collect();
        adjs.sort_unstable_by_key(|(_, adj_list)| adj_list.len());
        adjs.into_iter().rev().map(|(node, _)| node)
    }

    /// Returns the cut weight and the deficiency of a set of nodes.
    ///
    /// The cut weight of a vertex set S is the number of edges from nodes in S to nodes not in S.
    /// The deficiency is the number of anti-edges between two nodes in S.
    pub fn cut_weight_deficiency(&self, src_set: &HashSet<usize>) -> (usize, usize) {
        let mut cut_weight: usize = 0;
        let mut deficiency: usize = src_set.len() * (src_set.len() - 1);
        for i in src_set {
            if let Some(neighborhood) = &self.adj_list[*i] {
                for j in neighborhood {
                    if src_set.contains(&j) {
                        deficiency -= 1;
                    } else {
                        cut_weight += 1;
                    }
                }
            }
        }
        (cut_weight, deficiency / 2)
    }

    /// Returns the stable cost of a node and the number of edits it takes
    /// to make its closed neighborhood a cluster.
    ///
    /// The stable cost is the cut weight of its neighbourhood plus double the deficiency.
    /// See also [this method](`Self::cut_weight_deficiency()`).
    pub fn stable_cost(&self, node: usize) -> (usize, usize) {
        if let Some(open_neigh) = &self.adj_list[node] {
            let mut neighborhood: HashSet<_> = open_neigh.clone();
            neighborhood.insert(node);
            let (cw, def) = self.cut_weight_deficiency(&neighborhood);
            (2 * def + cw, def + cw)
        } else {
            (0, 0)
        }
    }
    /// Returns if (the neighborhood of) `node` is reducible.
    pub fn is_reducible(&self, node: usize) -> bool {
        self.stable_cost(node).0 <= self.degree(node)
    }
    /// Returns the anti-edges within a set of nodes.
    ///
    /// If `a` < `b` are in `src_set` and there is no edge between them,
    /// then `(a, b)` is in the output.
    pub fn anti_edges_within<'a: 'c, 'b: 'c, 'c>(&'a self, src_set: &'b HashSet<usize>) -> impl Iterator<Item=(usize, usize)> + 'c {
        src_set.iter()
            .filter(move |node| self.adj_list[**node].is_some())
            .flat_map(move |&node| {
                let targets = self.adj_list[node].as_ref().unwrap();
                let missing = src_set.difference(targets);
                missing.filter_map(move |&trg| {
                    if node < trg {
                        Some((node, trg))
                    } else { None }
                })
            })
    }

    /// Returns edges between `src_set` and the rest of the graph.
    ///
    /// Returned edges are of the format `(src, trg)` where `src` < `trg`
    pub fn edges_between_set_and_outside<'a: 'c, 'b: 'c, 'c>(&'a self, src_set: &'b HashSet<usize>) -> impl Iterator<Item=(usize, usize)> + 'c {
        src_set.iter()
            .filter(move |node| self.adj_list[**node].is_some())
            .flat_map(move |&node| {
                let targets = self.adj_list[node].as_ref().unwrap();
                let to_outside = targets.difference(src_set);
                to_outside.map(move |&trg| if node < trg {(node,trg)} else {(trg, node)})
            })
    }

    /// Returns edges incident to `node`.
    ///
    /// Returned edges are of the format `(src, trg)` where `src` < `trg`
    pub fn edges_at_node<'a: 'c, 'b: 'c, 'c>(&'a self, node: &'b usize) -> impl Iterator<Item=(usize, usize)> + 'c {
        let targets = self.adj_list[*node].as_ref().unwrap();
        targets.iter().map(move |&trg| if node < &trg {(*node,trg)} else {(trg, *node)})
    }

    /// Returns nodes of `set` that are adjacent to `node`.
    ///
    /// # Panics
    /// Panics if `node` is not a valid index of a (not deleted) node.
    pub fn adjacent_in<'a: 'c, 'b: 'c, 'c>(&'a self, node: usize, set: &'b HashSet<usize>) -> impl Iterator<Item=usize> + 'c {
        self.adj_list[node].as_ref().unwrap()
            .iter()
            .filter(move |trg| set.contains(trg))
            .cloned()
    }
    pub fn delete_edges_between(&mut self, node: usize, set: &HashSet<usize>) {
        for v in set {
            if self.adj_list[*v].as_mut().unwrap()
                .remove(&node) {
                self.edits.push(Edits::DeletedEdge((node, *v)));
            }
        }
        self.adj_list[node].as_mut().unwrap()
            .retain(|trg| !set.contains(trg));
    }
    /// Deletes a connected component from this graph.
    ///
    /// Leaves dangling edges if the provided set is not isolated from the rest of the graph.
    pub(crate) fn delete_component(&mut self, nodes: &HashSet<usize>) {
        let mut component: Vec<(usize, HashSet<usize>)> = Vec::new();
        for v in nodes {
            // TODO: this panics if a node in the component is already `None`
            component.push((*v, self.adj_list[*v].take().unwrap()));
        }
        self.edits.push(Edits::DeletedComp(component));
    }

    pub fn is_cluster_graph(&self) -> bool{
        let mut checked_nodes: Vec<usize> = Vec::new();
        for node in self.nodes() {
            if checked_nodes.contains(&node) {
                continue;
            }
            let mut neighborhood = self.open_neighborhood(node).as_ref().unwrap().clone();
            neighborhood.insert(node);

            for other in neighborhood.clone() {
                let mut other_neighborhood = self.open_neighborhood(other).as_ref().unwrap().clone();
                other_neighborhood.insert(other);
                if other_neighborhood != neighborhood {
                    eprintln!("Not a cluster: N[{}] != N[{}]", node+1, other+1);
                    eprintln!("{:?}", neighborhood.symmetric_difference(&other_neighborhood).map(|x| x+1).collect::<Vec<_>>());
                    return false;
                }
                checked_nodes.push(other)
            }
        }
        true
    }

    pub fn apply_solution(&mut self, solution: &Solution) {
        for (src, trg) in solution.flips() {
            if self.open_neighborhood(*src).as_ref().unwrap().contains(trg) {
                self.delete_edge((*src, *trg));
            } else {
                self.insert_edge((*src, *trg));
            }
        }
    }
}


impl UGraph {
    /// Creates a complete graph with `size` nodes.
    pub fn complete(size: usize) -> UGraph {
        let adj_list = {
            let mut a = Vec::with_capacity(size);

            for i in 0..size {
                let mut e: HashSet<usize> = Default::default();
                for j in 0..size {
                    if i == j { continue; }
                    e.insert(j);
                }
                a.push(Some(e));
            }
            a
        };

        UGraph {
            adj_list,
            ..Default::default()
        }
    }

    /// Creates an empty graph (that is, without edges) with `size` isolated nodes.
    pub fn empty(size: usize) -> UGraph {
        let adj_list = vec![Some(HashSet::new()); size];

        UGraph {
            adj_list,
            ..Default::default()
        }
    }

    /// Creates a cluster graph with indices less than `max_index` and with the specified clusters.
    ///
    /// Cluster that are just an isolated node may be omitted from `clusters`.
    pub fn with_clusters(max_index: usize, clusters: &[HashSet<usize>]) -> UGraph {
        let mut adj_list = vec![None; max_index];

        for c in clusters {
            for n in c {
                let neighbors = {
                    let mut nei = c.clone();
                    nei.remove(n);
                    nei
                };
                adj_list[*n] = Some(neighbors);
            }
        }

        UGraph {
            adj_list,
            ..Default::default()
        }
    }

    /// Create undirected graph from .gr file.
    // pub fn import_gr<P: AsRef<Path>>(gr: P) -> io::Result<UGraph> {
    //     let reader = my_reader::BufReader::open(gr)?;
    //     let mut reader = reader.skip_while(|s| {
    //         // assuming that the file contains more than comments
    //         s.as_ref().unwrap().to_string().starts_with('c')
    //     });
    //
    //     let line = reader.next().unwrap()?.to_string();
    //     let problem: Vec<&str> = line.split(' ').collect();
    //     let node_count: usize = problem[2].parse::<usize>().unwrap();
    //
    //     let mut adj_list: Vec<Option<HashSet<usize>>> = Vec::with_capacity(node_count);
    //
    //     // Assuming nodes are named 1, 2, 3 etc.
    //     for _ in 0..node_count {
    //         let empty_set = HashSet::new();
    //         adj_list.push(Some(empty_set));
    //     }
    //
    //     for remain_line in reader {
    //         let line = remain_line?.to_string();
    //         if line.starts_with('c') {
    //             continue;
    //         }
    //
    //         let edge_args: Vec<&str> = line.split(' ').collect();
    //         // Since node names start at 1
    //         let src = edge_args[0].parse::<usize>().unwrap() - 1;
    //         let trg = edge_args[1].trim().parse::<usize>().unwrap() - 1;
    //
    //         adj_list[src].as_mut().unwrap().insert(trg);
    //         adj_list[trg].as_mut().unwrap().insert(src);
    //     }
    //
    //     Ok(UGraph {
    //         adj_list,
    //         ..Default::default()
    //     })
    // }

    pub fn read_gr<R: BufRead>(gr: R) -> Result<UGraph, ImportError> {

        let (lines, comments): (Vec<_>, Vec<_>) = gr.lines()
            .partition(|l| {
                if let Ok(line) = l {
                    // ignore empty lines and comment lines
                    !line.starts_with("c ") && !line.is_empty()
                } else {
                    true
                }
            });

        let mut deleted_nodes: HashSet<usize> = HashSet::new();

        let mut records = Vec::new();
        for comment in comments {
            let comment: String = comment?;
            if comment.is_empty() {
                continue;
            }
            let comment = comment.strip_prefix("c ").unwrap();
            match comment.as_bytes()[0] {
                b'e' => {
                    let mut sol_lif = comment.split(' ');
                    sol_lif.next();
                    let src = sol_lif.next().ok_or(ImportError::Malformed)?.parse::<usize>()? - 1;
                    let trg = sol_lif.next().ok_or(ImportError::Malformed)?.parse::<usize>()? - 1;
                    records.push(SolutionLifting::FlipEdge((src, trg)));
                }
                b'b' => {
                    let sol_lif = comment.split(' ');
                    let mut belong: HashSet<usize> = HashSet::new();
                    let mut to: HashSet<usize> = HashSet::new();
                    let mut x = usize::MAX;

                    let mut state = 0;

                    for elem in sol_lif.skip(1) {
                        if elem == "t".to_string() {
                            state = 1;
                        }
                        if elem == "x".to_string(){
                            state = 2;
                        }
                        let node = elem.parse::<usize>()? - 1;

                        match state {
                            0 => {belong.insert(node);},
                            1 => {to.insert(node);},
                            2 => x = node,
                            _ => panic!("This cant be happening!"),
                        };
                    }
                    deleted_nodes.extend(belong.iter());

                    records.push(SolutionLifting::BelongsTo(belong, to, x));
                },
                b'c' => {
                    let sol_lif = comment.split(' ');
                    let cluster: HashSet<usize> = sol_lif.skip(1)
                        .map(|elem| elem.parse::<usize>().map(|i| i-1)).collect::<Result<_,_>>()?;
                    deleted_nodes.extend(cluster.iter());
                    records.push(SolutionLifting::MakeClique(cluster));
                },
                _ => (),
            }
        }


        let mut lines = lines.into_iter();

        // p cep <n> <m>
        let (n, m) = {
            let line = lines.next().ok_or(ImportError::Malformed)??;
            let mut s = line.split(' ');
            if let Some("p") = s.next() {} else { return Err(ImportError::Malformed); }
            if let Some("cep") = s.next() {} else { return Err(ImportError::Malformed); }
            let n: usize = s.next().ok_or(ImportError::Malformed)?.parse()?;
            let m: usize = s.next().ok_or(ImportError::Malformed)?.parse()?;
            if s.next().is_some() { return Err(ImportError::Malformed); }
            (n, m)
        };

        // Skipping deleted ids
        let mut deleted_nodes: Vec<usize> = deleted_nodes.into_iter().collect();
        deleted_nodes.sort_unstable();

        let mut adj_list = vec![Some(HashSet::new()); n+deleted_nodes.len()];
        for &i in &deleted_nodes {
            adj_list[i] = None;
        }

        // `add_to_id` indicates, for every node in the reduced graph, the number to add
        // in order to get the node name/index in the original graph
        let mut add_to_id = vec![0; n];
        let mut current_deleted_node = 0;
        let mut current_entry = 0;
        for (i, add) in add_to_id.iter_mut().enumerate() {
            while Some(&(i + current_entry)) == deleted_nodes.get(current_deleted_node) {
                current_entry += 1;
                current_deleted_node += 1;
            }
            *add = current_entry;
        }

        let mut num_edges = 0;
        for line in lines {
            // <src> <trg>
            let line = line?;
            let mut s = line.split(' ');
            let src = s.next().ok_or(ImportError::Malformed)?.parse::<usize>()? - 1;
            let trg = s.next().ok_or(ImportError::Malformed)?.parse::<usize>()? - 1;
            let src = src + add_to_id[src];
            let trg = trg + add_to_id[trg];
            if s.next().is_some() { return Err(ImportError::Malformed); }


            adj_list[src].as_mut().unwrap().insert(trg);
            adj_list[trg].as_mut().unwrap().insert(src);

            num_edges += 1;
        }
        if num_edges != m { return Err(ImportError::Malformed); }


        Ok(UGraph {
            adj_list,
            records,
            ..Default::default()
        })
    }


    /// Write su graph to .gr file.
    // pub fn export_gr<P: AsRef<Path>>(&self, path: P, print_mapping: bool) -> Result<(), io::Error> {
    //     let mut out = File::create(path)?;
    //
    //     // Problem definition TODO: (cep?)
    //     writeln!(out, "p cep {:?} {:?}", self.num_nodes(), self.num_edges())?;
    //
    //
    //     let mut i = 0;
    //     let node_name: Vec<_> = self.adj_list.iter()
    //         .map(|n| {
    //             if n.is_some() {
    //                 i += 1;
    //                 i
    //             } else {
    //                 123321 // dummy value
    //             }
    //         })
    //         .collect();
    //
    //     for edge in self.edges() {
    //         if print_mapping {
    //             writeln!(out, "c {} {}", edge.0 + 1, edge.1 + 1)?;
    //         }
    //         writeln!(out, "{} {}", node_name[edge.0], node_name[edge.1])?;
    //     }
    //
    //     Ok(())
    // }


    /// Write su graph as .gr file to a writer.
    pub fn write_gr<W: Write>(&self, mut out: W, print_mapping: bool) -> Result<(), io::Error> {

        let mut i = 0;
        let node_name: Vec<_> = self.adj_list.iter()
            .map(|n| {
                if n.is_some() {
                    i += 1;
                    i
                } else {
                    123321 // dummy value
                }
            })
            .collect();

        for sol_lif in &self.records {
            match sol_lif {
                SolutionLifting::FlipEdge((src, trg)) => writeln!(out, "c e {} {}", src+1, trg+1)?,
                SolutionLifting::MakeClique(cluster) => writeln!(out, "c c {}", cluster.iter()
                    .map(|node| format!("{} ",node+1)).collect::<String>().trim())?,
                SolutionLifting::BelongsTo(belong, to,x) => writeln!(out, "c b {}t {}x {}",
                belong.iter().map(|node| format!("{} ",node+1)).collect::<String>(),
                to.iter().map(|node| format!("{} ",node+1)).collect::<String>(),
                x+1)?,
            }
        }
        writeln!(out, "p cep {:?} {:?}", self.num_nodes(), self.num_edges())?;

        for edge in self.edges() {
            if print_mapping {
                writeln!(out, "c {} {}", edge.0 + 1, edge.1 + 1)?;
            }
            writeln!(out, "{} {}", node_name[edge.0], node_name[edge.1])?;
        }

        Ok(())
    }
}

/// Stores a set of nodes.
///
/// Implemented with a `Vec<usize>`. Trade-offs apply.
#[derive(Debug, Default, Clone)]
pub struct NodeSet {
    set: Vec<bool>,
    len: usize,
}
impl NodeSet {
    pub fn new() -> Self {
        Self {
            set: Vec::new(),
            len: 0,
        }
    }
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            set: Vec::with_capacity(capacity),
            len: 0,
        }
    }
    pub fn contains(&self, &n: &usize) -> bool {
        if n >= self.set.len() {
            false
        } else {
            self.set[n]
        }
    }
    /// Returns the number of elements in the set.
    pub fn len(&self) -> usize {
        self.len
    }
    /// Returns `true` if the set contains no elements.
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }
    /// Inserts an element into the set.
    pub fn insert(&mut self, n: usize) {
        if n < self.set.len() {
            if !self.set[n] {
                self.set[n] = true;
                self.len += 1;
            }
        } else {
            self.set.resize(n+1, false);
            self.set[n] = true;
            self.len += 1;
        }
    }
	/// Removes an element from the set
	pub fn remove(&mut self, n: usize) {
        if n < self.set.len() {
            if self.set[n] {
                self.set[n] = false;
                self.len -= 1;
            }
        }
	}
}
impl IntoIterator for NodeSet {
    type Item = usize;
    type IntoIter = NodeSetIntoIter;

    fn into_iter(self) -> Self::IntoIter {
        NodeSetIntoIter {
            set: self.set,
            len: self.len,
            i: 0,
        }
    }
}
impl FromIterator<usize> for NodeSet {
    fn from_iter<I: IntoIterator<Item=usize>>(iter: I) -> Self {
        let mut c = NodeSet {
            set: Vec::new(),
            len: 0,
        };
        for i in iter {
            c.insert(i);
        }
        c
    }
}
pub struct NodeSetIntoIter {
    set: Vec<bool>,
    len: usize,
    i: usize
}
impl Iterator for NodeSetIntoIter {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        if self.len == 0 { return None; }
        self.len -= 1;
        while !self.set[self.i] {
            self.i += 1;
        }
        self.i += 1;
        Some(self.i - 1)
    }
}
