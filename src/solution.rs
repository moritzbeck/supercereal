use crate::cust_error::ImportError;
use crate::alt_graph::{UGraph, SolutionLifting};

use std::io::{self, BufRead, Write};
use std::iter::FromIterator;
use std::collections::HashSet;

#[derive(Debug, Clone)]
enum CliqueIdentifier {
	Singelton(usize),
	MinMinClique(usize, usize, HashSet<usize>),
}

impl CliqueIdentifier {
	pub fn new(clique: &HashSet<usize>) -> Self {
		if clique.len() == 1 {
			CliqueIdentifier::Singelton(*clique.iter().clone().next().unwrap())
		} else {
			let mut min1 = usize::MAX;
			let mut min2 = usize::MAX;
			clique.iter().for_each(|v| {
				if v < &min2 {
					if v < &min1 {
						min2 = min1;
						min1 = *v;
					} else {
						min2 = *v;
					}
				}
			});
			CliqueIdentifier::MinMinClique(min1, min2, clique.clone())
		}
	}
}

// TODO: enforce order in which they are found
#[derive(Debug, Default, Clone)]
pub struct Solution {
	flips: Vec<(usize, usize)>,
}

impl PartialEq for Solution {
	fn eq(&self, other: &Self) -> bool {
		let elems = self.flips.iter().collect::<HashSet<_>>();
		let elems2 = other.flips.iter().collect::<HashSet<_>>();
		return elems == elems2
	}
}

impl Eq for Solution {

}

impl Solution {

	pub fn flips(&self) -> &Vec<(usize, usize)> {
		&self.flips
	}


	pub fn new() -> Self {
		Default::default()
	}

	pub fn with_edits(edits: Vec<(usize, usize)>) -> Self {
		Self {
			flips: edits
		}
	}

	pub fn remove_all_edits_from_and_inside(&mut self, set: &HashSet<usize>) {
		// get all the ids?
		let mut ids: Vec<usize> = self.flips.iter().enumerate().filter_map(|(i, (s,t))| {
			if set.contains(s) || set.contains(t) {
				Some(i)
			} else {
				None
			}
		}).collect();
		ids.sort_unstable();
		ids.iter().rev().for_each(|id| {self.flips.swap_remove(*id);})
	}

	pub fn len(&self) -> usize {
		self.flips.len()
	}

	/// Output solution means nothing until the ids have been fixed to the offset of the kernel.
	///
	/// Store edge flips as `(src, trg)` where `src` < `trg`
	pub fn read_solution<R: BufRead>(r: R) -> Result<Solution, ImportError> {
		let mut flips = Vec::new();

		for line in r.lines() {
			let line = line?;
			if line.is_empty() { continue; }
			let mut s = line.split(' ');
			let src = s.next().ok_or(ImportError::Malformed)?.parse::<usize>()? - 1;
			let trg = s.next().ok_or(ImportError::Malformed)?.parse::<usize>()? - 1;

			if src < trg {
				flips.push((src, trg));
			} else {
				flips.push((trg, src));
			}
		}

		Ok(Solution {
			flips
		})
	}
	pub fn write_solution<W: Write>(&self, mut w: W) -> Result<(), io::Error> {
		for (a, b) in &self.flips {
			writeln!(w, "{} {}", a+1, b+1)?;
		}
		Ok(())
	}

	/// Computes a solution to CE on `orig`, given a kernel of it and where `self` is an optimal solution of CE on `kernel`.
	pub fn lift_it(&self, orig: &UGraph, kernel: &UGraph) -> Solution {
		let mut self_copy = self.clone();

        // Final solution
		let mut solution = Solution::new();

		// Store cliques and their identifier (smallest + second smallest)
		let mut cliques: Vec<CliqueIdentifier> = Vec::new();

		// Record edge flips and final cliques:
		for r in kernel.records.iter().rev() {
			match r {
				SolutionLifting::FlipEdge((u, v)) => {
                    if self.flips.contains(&(*u, *v)) || self.flips.contains(&(*v, *u)) {
						// If edge was flipped in the given CE solution ignore both, the flip in the solution as well as the kernel flip.
						// Note that this can't happen, if the given CE solution is optimal.
						let i = self_copy.flips.iter().position(|&x| x == (*u, *v) || x == (*v, *u)).unwrap();
						self_copy.flips.swap_remove(i);
					} else {
                        // Note that if the same edge is later added to a cluster we do not mind the double flip.
                        if u < v {
							solution.flips.push((*u, *v));
						} else {
							solution.flips.push((*v, *u));
						}
					}
				}
				SolutionLifting::MakeClique(c) => {
					// The clique is added to a list of cliques for later processing.
					cliques.push(CliqueIdentifier::new(c));
				}
				SolutionLifting::BelongsTo(belong, to, x) => {
                    // Lifting here is a little complicated, since the CE solution might not be optimal.

					// Get smallest node id in `to` as an identifier.
                    let min_in_t = to.iter().min().unwrap();

					// Check if `to` is still in the kernel.
					if let Some(to_neighborhood) = kernel.open_neighborhood(*min_in_t) {

						// Check if x is still connected to t in kernel after solution.
						if to_neighborhood.contains( x) {

                            // Check if the edge `(min_in_t, x)` is flipped in solution.
                            // For this we use `self_copy`, since, if for any reason the edge is in `self` but not in `self_copy`, `to` and `x` can't be together in an optimal solution.
                            if self_copy.flips.contains(&(*min_in_t, *x)) || self_copy.flips.contains(&(*x, *min_in_t)) {
								// If so, `x` and `to` are separated in the solution.
								let mut cluster = HashSet::new();
								cluster.extend(to.into_iter());
								cluster.extend(belong.into_iter());
								// The cluster is added to a list of cliques for later processing.
								cliques.push(CliqueIdentifier::new(&cluster));

								// Remove all edge flips that contain either node in `to` from the solution.
								// This is done to avoid two flips that cancel each other out, resulting in incomplete cliques.
                                self_copy.remove_all_edits_from_and_inside(to);

							} else {
								let mut cluster = HashSet::new();
								cluster.extend(to.into_iter());
								cluster.insert(*x);

								// Remove all edge flips that contain either node in `to` + `x` from the solution.
								// This is done to avoid two flips that cancel each other out, resulting in incomplete cliques.
								self_copy.remove_all_edits_from_and_inside(&cluster);

								cluster.extend(belong.into_iter());
								// The cluster is added to a list of cliques for later processing.
								cliques.push(CliqueIdentifier::new(&cluster));

							}
						} else {
							// If `x` is not connected to `to` in `kernel`, it means that some other rule removed that edge, so `x` can't be in the same clique as `to` in an optimal solution.
							let mut cluster = HashSet::new();
							cluster.extend(to.into_iter());
							cluster.extend(belong.into_iter());
							// The cluster is added to a list of cliques for later processing.
							cliques.push(CliqueIdentifier::new(&cluster));

							// Remove all edge flips that contain either node in `to` from the solution.
							// This is done to avoid two flips that cancel each other out, resulting in incomplete cliques.
							self_copy.remove_all_edits_from_and_inside(to);
						}
					} else {
                        // If `to` is not in `kernel` find the cluster containing `to` in `cliques`:
						let pos = cliques.iter().position(|ci| {
							match ci {
								CliqueIdentifier::Singelton(elem) => elem == min_in_t,
								CliqueIdentifier::MinMinClique(min1, min2, _) => min1 == min_in_t || min2 == min_in_t,
							}
						});
						if let Some(pos) = pos {
							let ci = cliques.get(pos).unwrap();
							match ci {
								CliqueIdentifier::Singelton(_) => {
									// Remove `to` from `cliques`
									cliques.swap_remove(pos);
									// Add `belong` + `to` to `cliques`
									let mut cluster = HashSet::new();
									cluster.extend(to.into_iter());
									cluster.extend(belong.into_iter());
									cliques.push(CliqueIdentifier::new(&cluster));
								}
								CliqueIdentifier::MinMinClique(_, _, clique) => {
									// Check if `x` is in the same cluster as `to`.
                                    if clique.contains(x) {
										// Add `belong` + `to` + `x` to `cliques`
										cliques.swap_remove(pos);
										let mut cluster = HashSet::new();
										cluster.extend(to.into_iter());
										cluster.extend(belong.into_iter());
										cluster.insert(*x);
										cliques.push(CliqueIdentifier::new(&cluster));
									} else {
										// Add `belong` + `to` to `cliques`.
										cliques.swap_remove(pos);
										let mut cluster = HashSet::new();
										cluster.extend(to.into_iter());
										cluster.extend(belong.into_iter());
										cliques.push(CliqueIdentifier::new(&cluster));
									}
								}
							}
						} else {
							panic!("`to` not found")
						}
					}
				}
			}
		}

		// Process cluster in `cliques` and add flips to `solution`
		for clique_in_opt in cliques {
			match clique_in_opt {
				CliqueIdentifier::Singelton(single) => {
					// Remove all edges from `single` that remain in `orig`
                    let mut edges_to_remove = orig.edges_at_node(&single).collect::<Vec<(usize,usize)>>();
					solution.flips.append(&mut edges_to_remove);
				}
				CliqueIdentifier::MinMinClique(_, _, cluster) => {
					// Find anti edges in `orig` within `cluster`
					let mut missing_edges = orig.anti_edges_within(&cluster).collect::<Vec<(usize, usize)>>();
					// Find edges in `orig` between `cluster` and rest of graph
					let mut edges_to_remove = orig.edges_between_set_and_outside(&cluster).collect::<Vec<(usize, usize)>>();
					// Add flips to `solution`
					solution.flips.append(&mut missing_edges);
					solution.flips.append(&mut edges_to_remove);
				}
			}
		}

        // Add remainder of the kernel solution to `solution`.
		solution.flips.append(&mut self_copy.flips);
		solution.flips.sort_unstable();
		// Find and remove duplicate edge flips in `solution`.
		solution.flips.dedup();

        solution
	}
}
impl FromIterator<(usize, usize)> for Solution {
    fn from_iter<I: IntoIterator<Item=(usize, usize)>>(iter: I) -> Self {
        Solution { flips: Vec::from_iter(iter) }
    }
}

#[cfg(test)]
mod test {
	use super::Solution;
	use crate::alt_graph::UGraph;
	use std::io::Cursor;

	#[test]
	fn test_single_cluster() {
		let gr = Cursor::new("p cep 5 10\n1 2\n1 3\n1 4\n1 5\n2 3\n2 4\n2 5\n3 4\n3 5\n4 5\n");
		let graph = UGraph::read_gr(gr).unwrap();
		let gr = Cursor::new("p cep 0 0\nc c 1 2 3 4 5\n");
		let kernel = UGraph::read_gr(gr).unwrap();

		let empty_sol = Solution::read_solution(Cursor::new("\n")).expect("reads fine");
		assert_eq!(empty_sol.flips, Solution::new().flips);

		let lifted = empty_sol.lift_it(&graph, &kernel);
		assert_eq!(lifted.flips, vec![]);


		let gr = Cursor::new("p cep 5 9\n1 2\n1 3\n1 4\n2 3\n2 4\n2 5\n3 4\n3 5\n4 5");
		let graph = UGraph::read_gr(gr).unwrap();

		let lifted = empty_sol.lift_it(&graph, &kernel);
		assert_eq!(lifted.flips, vec![(0, 4)]);
	}
	#[test]
	fn test_cluster_plus_node() {
		let gr = Cursor::new("p cep 6 11\n1 2\n1 3\n1 4\n1 5\n2 3\n2 4\n2 5\n3 4\n3 5\n4 5\n5 6\n");
		let graph = UGraph::read_gr(gr).unwrap();
		let gr = Cursor::new("p cep 0 0\nc c 1 2 3 4 5\nc c 6");
		let kernel1 = UGraph::read_gr(gr).unwrap();
		let gr = Cursor::new("p cep 1 0\nc c 1 2 3 4 5\nc e 5 6"); // is this kernel legal?
		let kernel2 = UGraph::read_gr(gr).unwrap();
		let gr = Cursor::new("p cep 1 0\nc e 5 6\nc c 1 2 3 4 5\n");
		let kernel3 = UGraph::read_gr(gr).unwrap();
		let gr = Cursor::new("p cep 0 0\nc c 1 2 3 4 5\n"); // is this kernel legal?
		let kernel4 = UGraph::read_gr(gr).unwrap();
		let gr = Cursor::new("p cep 1 0\nc c 1 2 3 4 5\n");
		let kernel5 = UGraph::read_gr(gr).unwrap();

		let empty_sol = Solution::read_solution(Cursor::new("")).expect("reads fine");
		assert_eq!(empty_sol.flips, Solution::new().flips);

		let lifted = empty_sol.lift_it(&graph, &kernel1);
		assert_eq!(lifted.flips, vec![(4, 5)]);
		let lifted = empty_sol.lift_it(&graph, &kernel2);
		assert_eq!(lifted.flips, vec![(4, 5)]); // maybe not
		let lifted = empty_sol.lift_it(&graph, &kernel3);
		assert_eq!(lifted.flips, vec![(4, 5)]);
		let lifted = empty_sol.lift_it(&graph, &kernel4);
		assert_eq!(lifted.flips, vec![(4, 5)]);
		let lifted = empty_sol.lift_it(&graph, &kernel5);
		assert_eq!(lifted.flips, vec![(4, 5)]);
	}
	#[test]
	fn test_one_edge_flip() {
		let gr = Cursor::new("p cep 5 9\n1 2\n1 3\n1 4\n2 3\n2 4\n2 5\n3 4\n3 5\n4 5\n");
		let graph = UGraph::read_gr(gr).unwrap();
		let gr = Cursor::new("p cep 5 10\nc e 1 5\n1 2\n1 3\n1 4\n1 5\n2 3\n2 4\n2 5\n3 4\n3 5\n4 5\n");
		let kernel = UGraph::read_gr(gr).unwrap();

		let sol = Solution::new();

		let lifted = sol.lift_it(&graph, &kernel);
		assert_eq!(lifted.flips, vec![(0, 4)]);
	}
}
