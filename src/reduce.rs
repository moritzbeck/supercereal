use std::error;

use crate::{alt_graph::UGraph, bounds::{UpperBound, VertexOrder}};


pub fn reduce(mut graph: UGraph) -> Result<(usize, UGraph), Box<dyn error::Error>> {
	let mut edits = 0;

	eprint!("Edge Cuts...");
	let mut r_edits = 0;
	while let Some(v) = graph.find_reducible() {
		r_edits += graph.reduce(v);
	}
	eprintln!("\t({:>4} Nodes, {:>5} Edges) after {} Edits", graph.num_nodes(), graph.num_edges(), r_edits);
	edits += r_edits;

	eprint!("P_3-free...");
	let p_edits = graph.complete_p3_reduction();
	eprintln!("\t({:>4} Nodes, {:>5} Edges) after {} Edits", graph.num_nodes(), graph.num_edges(), p_edits);
	edits += p_edits;


	eprint!("\tUpper Bound... ");
	let mut bound = UpperBound::new(&graph);
	bound.compute_default_bounds();
	for _ in 0..10 {
		bound.greedy(VertexOrder::Random, 0.85);
		bound.greedy(VertexOrder::Random, 0.2);
	}
	bound.local_reassignments();
	let mut k = bound.upper_bound();
	eprintln!(" {}\t(total {})", k, k + edits);

	eprint!("\tLower Bound... ");
	let (mut is, lb) = {
		let mut is = graph.find_independent_conflict_set_ordered();
		for _ in 0..5 {
			let mut iset = graph.find_independent_conflict_set();
			graph.find_conflict_cycles(&mut iset);
			if iset.bound() > is.bound() {
				is = iset;
			}
		}
		//is.local_maximize(&graph); // takes too much time
		let lb = is.bound();
		(is, lb)
	};
	eprintln!(" {}\t(total {})", lb, lb + edits);

	if lb == k {
		eprintln!("\tBounds Match!");
		let clusters: Vec<_> = bound.clusters().cloned().collect();
		for c in clusters {
			graph.record_clique(&c);
			graph.delete_nodes(c);
		}

		edits += k;
		return Ok((edits, graph));
	}

	let max_deg = graph.max_degree();
	eprintln!("\tMax Degree... {}", max_deg);
	if k - lb > 2 * max_deg {
		eprintln!("Skipping (k+1) Rule");
		return Ok((edits, graph));
	}

	eprint!("(k+1) Rule...");
	let nodes: Vec<_> = graph.nodes().collect();
	let mut k_edits = 0;
	let mut heap = std::collections::BinaryHeap::new();
	for i in &nodes {
		for j in &nodes {
			if i == j { continue; }
			let diff = graph.do_the_k_plus_one((*i, *j), &mut is, k);
			if diff == 0 {
				// We edited an (anti-)edge that must be edited,
				// so we can reduce the upper bound by one.
				k_edits += 1;
				k -= 1;
			} else {
				heap.push((std::cmp::Reverse(diff), (*i, *j)));
			}
		}
	}
	let mut c = 10; // TODO: What value?
	while let Some((_, (i, j))) = heap.pop() {
		let diff = graph.do_the_k_plus_one((i, j), &mut is, k);
		if diff == 0 {
			// We edited an (anti-)edge that must be edited,
			// so we can reduce the upper bound by one.
			k_edits += 1;
			k -= 1;
		} else if c == 0 {
			break;
		} else {
			c -= 1;
		}
	}
	eprintln!("\t({:>4} Nodes, {:>5} Edges) after {} Edits", graph.num_nodes(), graph.num_edges(), k_edits);
	edits += k_edits;


	if k_edits == 0 {
		return Ok((edits, graph));
	}

	eprint!("\tUpper Bound... ");
	let mut bound = UpperBound::new(&graph);
	bound.compute_default_bounds();
	for _ in 0..10 {
		bound.greedy(VertexOrder::Random, 0.85);
		bound.greedy(VertexOrder::Random, 0.2);
	}
	bound.local_reassignments();
	let k = bound.upper_bound();
	eprintln!(" {}\t(total {})", k, k + edits);

	eprint!("\tLower Bound... ");
	let lb = {
		let mut is = graph.find_independent_conflict_set_ordered();
		for _ in 0..5 {
			let mut iset = graph.find_independent_conflict_set();
			graph.find_conflict_cycles(&mut iset);
			if iset.bound() > is.bound() {
				is = iset;
			}
		}
		//is.local_maximize(&graph); // takes too much time
		is.bound()
	};
	eprintln!(" {}\t(total {})", lb, lb + edits);

	if lb == k {
		eprintln!("\tBounds Match!");
		let clusters: Vec<_> = bound.clusters().map(|c| c.clone()).collect();
		for c in clusters {
			graph.record_clique(&c);
			graph.delete_nodes(c);
		}

		edits += k;
		return Ok((edits, graph));
	}

	//////////////////////////////////////////////////// new
	//////////////////////////////////////////////////// new
	//////////////////////////////////////////////////// new

	eprint!("Edge Cuts...");
	let mut r_edits = 0;
	while let Some(v) = graph.find_reducible() {
		r_edits += graph.reduce(v);
	}
	eprintln!("\t({:>4} Nodes, {:>5} Edges) after {} Edits", graph.num_nodes(), graph.num_edges(), r_edits);
	edits += r_edits;

	eprint!("\tUpper Bound... ");
	let mut bound = UpperBound::new(&graph);
	bound.compute_default_bounds();
	for _ in 0..10 {
		bound.greedy(VertexOrder::Random, 0.85);
		bound.greedy(VertexOrder::Random, 0.2);
	}
	bound.local_reassignments();
	let mut k = bound.upper_bound();
	eprintln!(" {}\t(total {})", k, k + edits);

	eprint!("\tLower Bound... ");
	let (mut is, lb) = {
		let mut is = graph.find_independent_conflict_set_ordered();
		for _ in 0..5 {
			let mut iset = graph.find_independent_conflict_set();
			graph.find_conflict_cycles(&mut iset);
			if iset.bound() > is.bound() {
				is = iset;
			}
		}
		//is.local_maximize(&graph); // takes too much time
		let lb = is.bound();
		(is, lb)
	};
	eprintln!(" {}\t(total {})", lb, lb + edits);

	if lb == k {
		eprintln!("\tBounds Match!");
		let clusters: Vec<_> = bound.clusters().map(|c| c.clone()).collect();
		for c in clusters {
			graph.record_clique(&c);
			graph.delete_nodes(c);
		}

		edits += k;
		return Ok((edits, graph));
	}

	eprint!("(k+1) Rule...");
	let nodes: Vec<_> = graph.nodes().collect();
	let mut k_edits = 0;
	let mut heap = std::collections::BinaryHeap::new();
	for i in &nodes {
		for j in &nodes {
			if i == j { continue; }
			let diff = graph.do_the_k_plus_one((*i, *j), &mut is, k);
			if diff == 0 {
				// We edited an (anti-)edge that must be edited,
				// so we can reduce the upper bound by one.
				k_edits += 1;
				k -= 1;
			} else {
				heap.push((std::cmp::Reverse(diff), (*i, *j)));
			}
		}
	}
	let mut c = 10; // TODO: What value?
	while let Some((_, (i, j))) = heap.pop() {
		let diff = graph.do_the_k_plus_one((i, j), &mut is, k);
		if diff == 0 {
			// We edited an (anti-)edge that must be edited,
			// so we can reduce the upper bound by one.
			k_edits += 1;
			k -= 1;
		} else if c == 0 {
			break;
		} else {
			c -= 1;
		}
	}
	eprintln!("\t({:>4} Nodes, {:>5} Edges) after {} Edits", graph.num_nodes(), graph.num_edges(), k_edits);
	edits += k_edits;


	eprint!("\tUpper Bound... ");
	let mut bound = UpperBound::new(&graph);
	bound.compute_default_bounds();
	for _ in 0..10 {
		bound.greedy(VertexOrder::Random, 0.85);
		bound.greedy(VertexOrder::Random, 0.2);
	}
	bound.local_reassignments();
	let k = bound.upper_bound();
	eprintln!(" {}\t(total {})", k, k + edits);

	eprint!("\tLower Bound... ");
	let lb = {
		let mut is = graph.find_independent_conflict_set_ordered();
		for _ in 0..5 {
			let mut iset = graph.find_independent_conflict_set();
			graph.find_conflict_cycles(&mut iset);
			if iset.bound() > is.bound() {
				is = iset;
			}
		}
		//is.local_maximize(&graph); // takes too much time
		is.bound()
	};
	eprintln!(" {}\t(total {})", lb, lb + edits);

	if lb == k {
		eprintln!("\tBounds Match!");
		let clusters: Vec<_> = bound.clusters().map(|c| c.clone()).collect();
		for c in clusters {
			graph.record_clique(&c);
			graph.delete_nodes(c);
		}

		edits += k;
		return Ok((edits, graph));
	}

	Ok((edits, graph))
}
